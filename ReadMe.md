# READ ME

本書では開発システムの概要、Git運用ルール、Gitのリポジトリ構成を定義する

## Quick summary

* プロジェクト毎にReadMeの内容は修正していくこと

## Git operational rules

##### 【開発作業の流れ】

masterブランチからdevelopブランチを作成  
developブランチから実装する機能毎にfeatureブランチを作成  
featureブランチで実装完了した機能はdevelopブランチにマージ  
リリース作業開始時点で、developからreleaseブランチを作成  
リリース作業完了時点で、releaseからdevelop, masterブランチにマージ  

##### 【リリース後の障害対応の流れ】
masterブランチからhotfixブランチを作成  
hotfixブランチで障害対応が完了した時点で、develop, masterブランチにマージ  

##### 【登場するブランチ】

| **ブランチ名** | **派生元** | **概要**                                                     |
| :------------- | :--------- | :----------------------------------------------------------- |
| master         | －         | リリースした時点のソースコードを管理するブランチ。 客先にリリースしたタイミングではTagを切って提供する。 |
| develop        | master     | 開発作業の主軸となるブランチ。次回リリースまでの期間はこのブランチで管理。 |
| release        | develop    | developでの開発作業完了後、リリース時の微調整を行うブランチ。 　(バージョン番号やデザイン微調整、難読化などで利用する) |
| hotfix         | master     | リリースされた製品に致命的なバグ(クラッシュなど)があった場合に緊急対応をするためのブランチ。 |
| feature        | develop    | 実装する機能毎or個人毎のブランチ。  (feature/◯◯, feature/xxなど) |

※詳細はConfluence参照のこと

##### 【gitコミットコメントルール】

gitのコミットコメントは下記のフォーマットとする。
スペースや改行も含めてルールとします。

```
<Prefix>: <Subject>

<Body>

<Footer>
```

コミットメッセージは4つの項目からなります。

1. Prefix 何をしたかを接頭辞で短くあらわします
2. Subject 何をしたかを短い文章にします
3. Body なぜそれをしたのかを文章にします
4. Footer 補足情報を載せます

```
＜記載例＞
Update: npmのパッケージをすべて最新版に更新する 

1年前に公開後、更新されていなかった。 
改修が始まるため、使用するパッケージやライブラリも新しいバージョンを使うようにする 

#test
```
※詳細はConfluence参照のこと

## Structure

root  
├─1_Design                          ：設計関係の資料  
│  ├─10_img                          ：仕様書・設計書の画像ファイル  
│  ├─11_FunctionalSpecification.md   ：機能仕様書  
│  ├─12_ArchitectureDesign.md        ：アーキテクチャ設計書  
│  └─13_ScreenDesign.md              ：詳細設計書  
│  
├─2_Program                             ：プログラム本体  
│  ├─FirebaseApp                         ：Firebase Project開発環境  
│  │  ├─ProjA                             ：Project用（フォルダ名は任意）  
│  │  │  ├─func                            ：functions用コード群  
│  │  │  │  ├─functions                     ：functionsソースコード群  
│  │  │  │  │  ├─.vscode                     ：VSCodeの設定ファイル  
│  │  │  │  │  ├─draw                        ：描画モジュール  
│  │  │  │  │  ├─func                        ：機能モジュール  
│  │  │  │  │  ├─htmlcss                     ：htmlcssモジュール  
│  │  │  │  │  ├─linkage                     ：外部システム連携モジュール  
│  │  │  │  │  ├─node_modules                ：開発用モジュール群（基本は自動生成）  
│  │  │  │  │  ├─index.js                    ：メインファイル。必ずここを入口にする  
│  │  │  │  │  └─package.json                ：Nodejs用のパッケージ設定  
│  │  │  │  ├─.firebaserc                   ：Firebase Project設定ファイル  
│  │  │  │  └─firebase.json                 ：Firebase用の詳細説明設定ファイル  
│  │  │  └─host                            ：hosting用コード群  
│  │  │      ├─node_modules                  ：開発用モジュール群（基本は自動生成）  
│  │  │      ├─public                        ：hosting本体  
│  │  │      ├─src                           ：開発ソースコード群  
│  │  │      │  ├─.vscode                     ：VSCodeの設定ファイル  
│  │  │      │  ├─apps                        ：hosting用ソースコード  
│  │  │      │  │  ├─appA                      ：webpackで固める塊（フォルダ名は任意）  
│  │  │      │  │  │  ├─draw                    ：描画モジュール  
│  │  │      │  │  │  ├─func                    ：機能モジュール  
│  │  │      │  │  │  ├─htmlcss                 ：htmlcssモジュール  
│  │  │      │  │  │  ├─linkage                 ：外部システム連携モジュール  
│  │  │      │  │  │  └─index.js                ：メインファイル。必ずここを入口にする  
│  │  │      │  │  └─appB                      ：webpackで固める塊（フォルダ名は任意。以下は同一）  
│  │  │      │  ├─common                      ：webpack用commonソースコード  
│  │  │      │  └─vendor                      ：webpack用ベンダー提供のモジュール  
│  │  │      ├─.eslintrc.js                  ：スペルチェックJavascriptファイル **<u>※触らない</u>**  
│  │  │      ├─.firebaserc                   ：Firebase Project設定ファイル  
│  │  │      ├─firebase.json                 ：Firebase用の詳細説明設定  
│  │  │      ├─package.json                  ：Nodejs用のパッケージ設定  
│  │  │      ├─uploader.js                   ：アップロードJavascriptファイル **<u>※触らない</u>**  
│  │  │      └─webpack.config.js             ：webpack設定ファイル  
│  │  └─ProjB                             ：Project用（フォルダ名は任意。以下は同一）  
│  │  
│  ├─kintone                             ：kintone関連の開発環境  
│  │  ├─kintoneCustom                     ：kintoneAppカスタマイズ開発環境  
│  │  │  ├─dist                            ：Build後のkintoneAppカスタマイズソース(自動生成)  
│  │  │  ├─node_modules                    ：開発用モジュール群（基本は自動生成）  
│  │  │  ├─src                             ：開発ソースコード群  
│  │  │  │  ├─.vscode                       ：VSCodeの設定ファイル  
│  │  │  │  ├─apps                          ：kintoneAppカスタマイズソースコード  
│  │  │  │  │  ├─app1                        ：kintoneApp用（フォルダ名は任意）  
│  │  │  │  │  │  ├─draw                      ：描画モジュール  
│  │  │  │  │  │  ├─func                      ：機能モジュール  
│  │  │  │  │  │  ├─htmlcss                   ：htmlcssモジュール  
│  │  │  │  │  │  ├─linkage                   ：外部システム連携モジュール  
│  │  │  │  │  │  ├─index.js                  ：メインファイル。必ずここを入口にする  
│  │  │  │  │  │  └─customize-manifest.json   ：kintoneApp マニフェストファイル  
│  │  │  │  │  └─app2                        ：kintoneApp用（フォルダ名は任意。以下は同一）  
│  │  │  │  ├─common                        ：kintoneApp用commonソースコード  
│  │  │  │  └─vendor                        ：kintoneApp用ベンダー提供のモジュール  
│  │  │  ├─.env                            ：kintoneログイン情報の記述用  
│  │  │  ├─.eslintrc.js                    ：スペルチェックJavascriptファイル **<u>※触らない</u>**  
│  │  │  ├─LICENSE                         ：ライセンスファイル **<u>※触らない</u>**  
│  │  │  ├─package.json                    ：Nodejs用のパッケージ設定  
│  │  │  ├─uploader.js                     ：アップロードJavascriptファイル **<u>※触らない</u>**  
│  │  │  └─webpack.config.js               ：webpackの設定ファイル  
│  │  │  
│  │  └─kintonePlugin                     ：kintonePlugin群  
│  │      ├─PluginA                         ：kintonePluginソースコード（フォルダ名は任意）  
│  │      │  ├─dist                          ：Build後ファイル(Plugin.zip)出力場所(自動生成)  
│  │      │  ├─node_modules                  ：開発用モジュール群（基本は自動生成）  
│  │      │  ├─plugin                        ：Pluginパッケージ化ソースコード  
│  │      │  │  ├─css                         ：パッケージ用CSS  
│  │      │  │  ├─html                        ：パッケージ用html  
│  │      │  │  ├─img                         ：パッケージ用画像  
│  │      │  │  ├─js                          ：パッケージ用JavaScript（自動生成）  
│  │      │  │  └─manifest.json               ：kintonePlguin用マニフェストファイル  
│  │      │  ├─src                           ：コンフィグ用Javascriptファイル  
│  │      │  │  ├─.vscode                     ：VSCodeの設定ファイル  
│  │      │  │  ├─common                      ：描画モジュール  
│  │      │  │  ├─config                      ：コンフィグ用ファイル群  
│  │      │  │  │  ├─draw                      ：描画モジュール  
│  │      │  │  │  ├─func                      ：機能モジュール  
│  │      │  │  │  ├─htmlcss                   ：htmlcssモジュール  
│  │      │  │  │  ├─linkage                   ：外部システム連携モジュール  
│  │      │  │  │  └─index.js                  ：メインファイル。必ずここを入口にする  
│  │      │  │  ├─desktop                     ：デスクトップ用ファイル群（構成はconfigと同一）  
│  │      │  │  ├─mobile                      ：モバイル用ファイル群（構成はconfigと同一）  
│  │      │  │  └─vendor                      ：kintonePlguin用ベンダー提供のモジュール  
│  │      │  ├─.env                          ：kintoneログイン情報の記述用  
│  │      │  ├─package.json                  ：Nodejs用のパッケージ設定  
│  │      │  ├─private.ppk                   ：Pluginのパッケージ化key(自動生成)  
│  │      │  ├─uploader.js                   ：アップロードJavascriptファイル **<u>※触らない</u>**  
│  │      │  └─webpack.config.js             ：webpackの設定ファイル  
│  │      └─PluginB                         ：kintonePluginソースコード（フォルダ名は任意。以下は同一）  
│  │  
│  ├─nodejsApp                           ：NodeApp用ソースコード一式  
│  │  ├─App1                              ：NodejsApp用のソースコード（フォルダ名は任意）  
│  │  │  ├─node_modules                    ：開発用モジュール群（基本は自動生成）  
│  │  │  ├─src                             ：開発ソースコード群  
│  │  │  │  ├─.vscode                       ：VSCodeの設定ファイル  
│  │  │  │  ├─common                        ：commonモジュール  
│  │  │  │  ├─draw                          ：描画モジュール  
│  │  │  │  ├─func                          ：機能モジュール  
│  │  │  │  ├─htmlcss                       ：htmlcssモジュール  
│  │  │  │  ├─linkage                       ：外部システム連携モジュール  
│  │  │  │  ├─vendor                        ：ベンダー提供のモジュール  
│  │  │  │  └─index.js                      ：メインファイル。必ずここを入口にする  
│  │  │  └─package.json                    ：Nodejs用のパッケージ設定  
│  │  └─App2                              ：NodejsApp用のソースコード（フォルダ名は任意。以下は同一）  
│  │  
│  ├─.gitignore                          ：git登録除外ファイル設定  
│  ├─FirebaseProjA_func.code-workspace   ：kintoneCustom用のVisualStudioCode環境  
│  ├─FirebaseProjA_host.code-workspace   ：kintoneCustom用のVisualStudioCode環境  
│  ├─kintoneCustom.code-workspace        ：kintoneCustom用のVisualStudioCode環境  
│  └─kintonePluginpluginA.code-workspace ：kintoneCustom用のVisualStudioCode環境  
│  
├─3_Test                              ：テスト関係の資料  
│  ├─30_TestingEnvironment               ：テスト環境  
│  ├─31_TestSpecification                ：テスト仕様書  
│  ├─32_TestResult                       ：テスト結果  
│  └─33_TestSummary                      ：テストまとめ  
│  
├─4_Release                           ：リリース資料  
│  ├─40_OperationalEnvironment           ：運用環境  
│  ├─41_BuildEnvironment                 ：ビルド環境  
│  ├─42_ReleaseModule                    ：リリースモジュール一式  
│  └─43_Manual                           ：操作マニュアル  
│  
├─5_Operation                         ：運用関係の資料  
│  └─51_OperationManual                  ：運用マニュアル  
│  
├─6_Other                             ：その他資料  
│  └─61_minutes                          ：客先提出議事録  
│      ├─img                          ：客先提出議事録用の画像ファイル  
│      ├─yy-mm-dd_XXXXXXXX.md           ：客先提出議事録（日付毎にファイル作成）  
│      └─yy-mm-dd_templete.md           ：客先提出議事録のテンプレート  
│  
└─ReadMe.md                           ：本書  

### Development environment setup

1.  [XXXX.code-workspace]をダブルクリックし、VSCodeを起動する  
2.  [ターミナル > タスクの実行 > init]を押下する  
　　 ⇒ 開発環境に必要なモジュールなどがインストールされます  
3. （kintoneのみ）各フォルダ内の[.env]ファイルにドメイン・ユーザー名・パスワードを記載する  
　　(.env_bsをコピー＆リネームして使用する)  
　　 ⇒ 各ソースコードなどをkintone環境にUPする際に必要です。  

以上でSETUPは終了です。

### webpack develop build @kintone customize, kintone plugin, firebase hosting

1. VScodeの[ターミナル > タスクの実行 > develop]を押下する  
     ⇒ 各環境のbuild後出力フォルダにbuildされたJavaScriptファイルが出来上がります。  

### webpack release build @kintone customize, kintone plugin, firebase hosting

1. VScodeの[ターミナル > タスクの実行 > build]を押下する  
     ⇒ 各環境のbuild後出力フォルダにbuildされたJavaScriptファイルが出来上がります。  

### build files upload @kintone customize, kintone plugin

1. VScodeの[ターミナル > タスクの実行 > upload]を押下する  
     ⇒ 各環境の完成ファイルが、[.env]ファイルのドメインにuploadされます。  

