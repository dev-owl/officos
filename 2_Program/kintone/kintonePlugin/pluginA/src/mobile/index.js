(function(str_a_PLUGIN_ID) {
    'use strict';
    kintone.plugin.app.getConfig(str_a_PLUGIN_ID);
    kintone.events.on('mobile.app.record.index.show', function (obj_a_event) {
        /* funcA(); */
    });
    kintone.events.on('mobile.app.record.detail.show', function (obj_a_event) {
        /* funcA(); */
    });
})(kintone.$PLUGIN_ID);
