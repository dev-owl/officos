import { execSync } from 'child_process';

const command = `npx kintone-plugin-uploader --domain ${process.env.KINTONE_DOMAIN} --username ${process.env.KINTONE_USER} --password ${process.env.KINTONE_PASSWORD} dist/plugin.zip  --watch --waiting-dialog-ms 3000`;
const result = execSync(command);
console.log('\n' + result);