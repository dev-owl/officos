const TERMENDMONTH = 2;  /**期末の月 */

/**
 * 日付から月初を算出する
 * @module str_g_GetMonthStartDay
 * @param {String} str_a_refday -基準日
 * @returns {String} -月開始日
 */
export function str_g_GetMonthStartDay(str_a_refday) {

    let str_s_rtn;  /**戻り値 */
    let num_s_year = Number(str_a_refday.substr(0, 4));
    let num_s_month = Number(str_a_refday.substr(5, 2));

    /**月の0埋め処理 */
    let str_s_month = ('00' + num_s_month).slice(-2);
    str_s_rtn = `${num_s_year}-${str_s_month}-01`;

    return str_s_rtn;
}

/**
 * 日付から月末を算出する
 * @module str_g_GetMonthEndDay
 * @param {String} str_a_refday -基準日
 * @returns {String} -月最終日
 */
export function str_g_GetMonthEndDay(str_a_refday) {

    let str_s_rtn;  /**戻り値 */
    let num_s_year = Number(str_a_refday.substr(0, 4));
    let num_s_month = Number(str_a_refday.substr(5, 2));
    let dat_s_day = new Date(num_s_year, num_s_month, 0);
    let str_s_day = dat_s_day.getDate();

    /**月日の0埋め処理 */
    let str_s_month = ('00' + num_s_month).slice(-2);
    str_s_rtn = `${num_s_year}-${str_s_month}-${str_s_day}`;

    return str_s_rtn;
}

/**
 * 日付から年度開始日付を算出する
 * @module str_g_GetYearStartDay
 * @param {String} str_a_refday -基準日
 * @returns {String} -年度開始日
 */
export function str_g_GetYearStartDay(str_a_refday) {

    let str_s_rtn;  /**戻り値 */
    let num_s_year = Number(str_a_refday.substr(0, 4));
    let num_s_month = Number(str_a_refday.substr(5, 2));

    if (num_s_month <= TERMENDMONTH) {
        /**年度用にデクリメント */
        num_s_year--;
    }
    /**月の0埋め処理 */
    let str_s_month = ('00' + (TERMENDMONTH + 1)).slice(-2);
    str_s_rtn = `${num_s_year}-${str_s_month}-01`;

    return str_s_rtn;
}

/**
 * 日付から年度終了日付を算出する
 * @module str_g_GetYearEndDay
 * @param {String} str_a_refday -基準日
 * @returns {String} -年度最終日
 */
export function str_g_GetYearEndDay(str_a_refday) {

    let str_s_rtn;  /**戻り値 */
    let num_s_year = Number(str_a_refday.substr(0, 4));
    let num_s_month = Number(str_a_refday.substr(5, 2));

    if (num_s_month > TERMENDMONTH) {
        /**年度用にインクリメント */
        num_s_year++;
    }
    let dat_s_day = new Date(num_s_year, TERMENDMONTH, 0);
    let str_s_day = dat_s_day.getDate();

    /**月日の0埋め処理 */
    let str_s_month = ('00' + TERMENDMONTH).slice(-2);
    str_s_rtn = `${num_s_year}-${str_s_month}-${str_s_day}`;

    return str_s_rtn;
}