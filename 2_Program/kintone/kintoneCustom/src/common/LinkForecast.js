import appIds from './app_ids'
import { str_g_GetMonthStartDay, str_g_GetMonthEndDay, str_g_GetYearStartDay, str_g_GetYearEndDay } from './CompanyCfg'

const cls_g_app = new appIds();

/**
 * 月間実績レコード更新
 * @module vd_g_putMonthProg
 * @param {Number} num_a_id -対象ID
 * @param {Object} obj_a_param -レコードの更新情報
 * @return {} -なし
 */
export async function vd_g_putMonthProg(num_a_id, obj_a_param) {

  let obj_s_rqparm = {
    "app": cls_g_app.Forecast(),
    "id": num_a_id,
    "record": obj_a_param
  };
  console.log(obj_s_rqparm);

  let obj_s_resp = await kintone.api(kintone.api.url('/k/v1/record', true), 'PUT', obj_s_rqparm);

  return;
}

/**
 * 月間実績レコード情報取得
 * @module obj_g_getMonthRecInfo
 * @param {String} str_a_refday -基準日
 * @param {Object} obj_a_param -レコードの更新情報
 * @return {Object} -レコード番号とルックアップ情報
 */
export async function obj_g_getMonthRecInfo(str_a_refday, str_a_user) {

  let obj_s_rtn = { "recid": 0 };
  let obj_s_rqparm = {
    "app": cls_g_app.Forecast(),
    "query": `usr_営業担当者 in ("${str_a_user}") and dat_対象年月bs = "${str_a_refday}"`,
    "totalCount": true
  };

  let obj_s_resp = await kintone.api(kintone.api.url('/k/v1/records', true), 'GET', obj_s_rqparm);

  if (obj_s_resp.totalCount != 0) {
    obj_s_rtn.recid = obj_s_resp.records[0].$id.value;
    obj_s_rtn.lok_予算情報取得 = obj_s_resp.records[0].lok_予算情報取得.value;
  }

  return obj_s_rtn;
}

/**
 * 年間実績レコード更新
 * @module vd_g_putYearProg
 * @param {Number} num_a_id -対象ID
 * @param {Object} obj_a_param -レコードの更新情報
 * @return {} -なし
 */
export async function vd_g_putYearProg(num_a_id, obj_a_param) {

  let obj_s_rqparm = {
    "app": cls_g_app.Budget(),
    "id": num_a_id,
    "record": obj_a_param
  };
  console.log(obj_s_rqparm);

  let obj_s_resp = await kintone.api(kintone.api.url('/k/v1/record', true), 'PUT', obj_s_rqparm);

  return;
}

/**
 * 年間実績レコード取得
 * @module num_g_getYearRecID
 * @param {Number} num_a_id -対象ID
 * @param {Object} obj_a_param -レコードの更新情報
 * @return {Number} -レコード番号
 */
export async function num_g_getYearRecID(num_a_year, str_a_user) {

  let num_t_rtn = 0;
  let obj_s_rqparm = {
    "app": cls_g_app.Budget(),
    "query": `usr_営業担当者 in ("${str_a_user}") and num_年度 = ${num_a_year}`,
    "totalCount": true
  };

  let obj_s_resp = await kintone.api(kintone.api.url('/k/v1/records', true), 'GET', obj_s_rqparm);

  if (obj_s_resp.totalCount != 0) {
    num_t_rtn = obj_s_resp.records[0].$id.value;
  }

  return num_t_rtn;
}

/**
 * 契約売上情報の合計値を算出する
 * @module num_g_CalcConstSales
 * @param {String} str_s_usercode 
 * @param {Date} dat_a_recday -
 * @param {Boolean} bln_a_yearflg -年間集計フラグ
 */
export async function num_g_CalcConstSales(str_s_usercode, dat_a_recday, bln_a_yearflg) {

  let str_s_stday;  /**集計対象開始日 */
  let str_s_endday; /**集計対象終了日 */

  /**検索開始終了日の計算 */
  if (bln_a_yearflg == true) {
    str_s_stday = await str_g_GetYearStartDay(dat_a_recday);
    str_s_endday = await str_g_GetYearEndDay(dat_a_recday);
  }
  else {
    str_s_stday = await str_g_GetMonthStartDay(dat_a_recday);
    str_s_endday = await str_g_GetMonthEndDay(dat_a_recday);
  }

  let obj_s_rqparm = {
    "app": cls_g_app.Contract(),
    "query": `usr_営業担当者 in ("${str_s_usercode}") and day_売上計上日 >= "${str_s_stday}" and day_売上計上日 <= "${str_s_endday}"`,
    "totalCount": true
  };
  let obj_s_resp = await kintone.api(kintone.api.url('/k/v1/records', true), 'GET', obj_s_rqparm);
  let obj_s_rtn = {};
  obj_s_rtn.tatalsales = await num_s_CalcSum(obj_s_resp.records, 'aut_売上', 'usr_営業担当者');
  obj_s_rtn.totalcnt = await num_s_CalcCnt(obj_s_resp.records, 'usr_営業担当者');

  return obj_s_rtn;
}

/**
 * その他売上情報の合計値を算出する
 * @module num_g_CalcOtherSales
 * @param {String} str_s_usercode 
 * @param {Date} dat_a_recday -基準日
 * @param {Boolean} bln_a_yearflg -年間集計フラグ
 */
export async function num_g_CalcOtherSales(str_s_usercode, dat_a_recday, bln_a_yearflg) {

  let str_s_stday;  /**集計対象開始日 */
  let str_s_endday; /**集計対象終了日 */

  /**検索開始終了日の計算 */
  if (bln_a_yearflg == true) {
    str_s_stday = await str_g_GetYearStartDay(dat_a_recday);
    str_s_endday = await str_g_GetYearEndDay(dat_a_recday);
  }
  else {
    str_s_stday = await str_g_GetMonthStartDay(dat_a_recday);
    str_s_endday = await str_g_GetMonthEndDay(dat_a_recday);
  }

  let obj_s_rqparm = {
    "app": cls_g_app.OtherSale(),
    "query": `usr_営業担当者 in ("${str_s_usercode}") and day_売上計上日 >= "${str_s_stday}" and day_売上計上日 <= "${str_s_endday}"`,
    "totalCount": true
  };

  let obj_s_resp = await kintone.api(kintone.api.url('/k/v1/records', true), 'GET', obj_s_rqparm);
  let obj_s_rtn = {};
  obj_s_rtn.tatalsales = await num_s_CalcSum(obj_s_resp.records, 'num_売上金額', 'usr_営業担当者');
  obj_s_rtn.totalcnt = await num_s_CalcCnt(obj_s_resp.records, 'usr_営業担当者');

  return obj_s_rtn;
}

/**
 * 案件情報の内見実施数を算出する
 * @module num_g_CalcMatterGuide
 * @param {String} str_s_usercode 
 * @param {Date} dat_a_recday -基準日
 * @param {Boolean} bln_a_yearflg -年間集計フラグ
 */
 export async function num_g_CalcMatterGuide(str_s_usercode, dat_a_recday, bln_a_yearflg) {

  let str_s_stday;  /**集計対象開始日 */
  let str_s_endday; /**集計対象終了日 */

  /**検索開始終了日の計算 */
  if (bln_a_yearflg == true) {
    str_s_stday = await str_g_GetYearStartDay(dat_a_recday);
    str_s_endday = await str_g_GetYearEndDay(dat_a_recday);
  }
  else {
    str_s_stday = await str_g_GetMonthStartDay(dat_a_recday);
    str_s_endday = await str_g_GetMonthEndDay(dat_a_recday);
  }

  let obj_s_rqparm = {
    "app": cls_g_app.Matter(),
    "query": `usr_営業担当者 in ("${str_s_usercode}") and day_問合せ日 >= "${str_s_stday}" and day_問合せ日 <= "${str_s_endday}" and txt_内見 = "実施"`,
    "totalCount": true
  };

  let obj_s_resp = await kintone.api(kintone.api.url('/k/v1/records', true), 'GET', obj_s_rqparm);
  let obj_s_rtn = {};
  obj_s_rtn.totalcnt = await num_s_CalcCnt(obj_s_resp.records, 'usr_営業担当者');

  return obj_s_rtn;
}

/**
 * 抽出レコードの対象フィールド合計値の算出
 * @module num_s_CalcSum
 * @param {Object} obj_a_record -レコード情報
 * @param {String} str_a_key -集計対象key
 * @param {String} str_a_userkey -ユーザー選択対象key
 * @return {Number} -集計結果
 */
function num_s_CalcSum(obj_a_record, str_a_key, str_a_userkey) {

  let num_s_total = 0;

  for (let i = 0; i < obj_a_record.length; i++) {
    /**ユーザー選択対象keyが複数の場合は除算してから加算する */
    let num_t_usercnt = obj_a_record[i][str_a_userkey].value.length;
    num_s_total += (Number(obj_a_record[i][str_a_key].value) / num_t_usercnt);
  }

  return num_s_total;
}

/**
 * 件数の算出
 * @module num_s_ContrctCnt
 * @param {Object} obj_a_record -レコード情報
 * @param {String} str_a_userkey -ユーザー選択対象key
 * @return {Number} -件数
 */
async function num_s_CalcCnt(obj_a_record, str_a_userkey) {

  let num_s_cnt = 0;

  for (let i = 0; i < obj_a_record.length; i++) {
    /**ユーザー選択対象keyが複数の場合は除算してから加算する */
    let num_t_usercnt = obj_a_record[i][str_a_userkey].value.length;
    num_s_cnt += (1 / num_t_usercnt);
  }

  return num_s_cnt;
}