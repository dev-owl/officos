/**
 * 
 * @param {*} obj_a_record 
 * @param {*} str_a_targetfld 
 * @param {*} str_a_val 
 * @returns 
 */
export function vd_g_fieldchg(obj_a_record, str_a_targetfld, str_a_val) {

    obj_a_record[str_a_targetfld].value = str_a_val;

    return obj_a_record;
}