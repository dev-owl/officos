const bln_g_DEBUGFLG = true;

export default class appIds {
  applist() { return; } /**開発環境用 */
  Budget() { return bln_g_DEBUGFLG === true ? 303 : 22; }        /**年間予算入力アプリ */
  ClassMaster() { return bln_g_DEBUGFLG === true ? 296 : 24; }   /**区分マスタアプリ */
  Contract() { return bln_g_DEBUGFLG === true ? 297 : 23; }      /**契約完了アプリ */
  Customer() { return bln_g_DEBUGFLG === true ? 299 : 19; }      /**顧客情報アプリ */
  Forecast() { return bln_g_DEBUGFLG === true ? 304 : 21; }      /**月別予算入力アプリ */
  Matter() { return bln_g_DEBUGFLG === true ? 298 : 18; }        /**案件管理アプリ */
  OtherSale() { return bln_g_DEBUGFLG === true ? 305 : 15; }     /**内装売上入力アプリ */
  OtherPartic() { return bln_g_DEBUGFLG === true ? 301 : 16; }   /**オーナー・他業者情報アプリ */
  Report() { return bln_g_DEBUGFLG === true ? 300 : 17; }        /**営業日報アプリ */
  Trader() { return bln_g_DEBUGFLG === true ? 302 : 20; }        /**業者情報アプリ */

  Debflg() { return bln_g_DEBUGFLG }
}