import { vd_g_ProgUpdate } from './func/ProgUpdate';
import { vd_g_DisApplinkFild } from '../../common/AllFieldCtr'

(function () {
  'use strict';

  /**レコード一覧画面表示時のイベント */
  kintone.events.on('app.record.index.show', function (obj_a_event) {
    let obj_s_record = sessionStorage.getItem('ContProgCal');
    if (obj_s_record != null) {
      vd_g_ProgUpdate(JSON.parse(obj_s_record));
      sessionStorage.removeItem('ContProgCal');
    }
  });

  /**レコード追加画面表示時のイベント */
  kintone.events.on('app.record.create.show', function (obj_a_event) {
    vd_g_DisApplinkFild();
  });

  /**レコード編集画面表示時のイベント */
  kintone.events.on('app.record.edit.show', function (obj_a_event) {
    vd_g_DisApplinkFild();
  });

  /**レコード一覧画面のインライン編集の保存成功後イベント */
  kintone.events.on('app.record.index.edit.submit.success', async function (obj_a_event) {
    let obj_s_await = await vd_g_ProgUpdate(obj_a_event.record);
  });

  /**レコード編集画面の保存成功後イベント */
  kintone.events.on('app.record.edit.submit.success', async function (obj_a_event) {
    let obj_s_await = await vd_g_ProgUpdate(obj_a_event.record);
  });

  /**レコード詳細画面表示時のイベント */
  kintone.events.on('app.record.detail.show', function (obj_a_event) {
    vd_g_DisApplinkFild();
  });

  /**レコード追加画面の保存成功後イベント */
  kintone.events.on('app.record.create.submit.success', async function (obj_a_event) {
    let obj_s_await = await vd_g_ProgUpdate(obj_a_event.record);
  });

  /**レコード一覧画面からレコード削除時のイベント */
  kintone.events.on('app.record.index.delete.submit', async function (obj_a_event) {
    /**削除後の再集計情報を設定する */
    sessionStorage.setItem('ContProgCal', JSON.stringify(obj_a_event.record));
  });
  
  /**レコード詳細画面からレコード削除時のイベント */
  kintone.events.on('app.record.detail.delete.submit', async function (obj_a_event) {
    /**削除後の再集計情報を設定する */
    sessionStorage.setItem('ContProgCal', JSON.stringify(obj_a_event.record));
  });

})();