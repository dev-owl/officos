import appIds from '../../../common/app_ids';
import { str_g_GetMonthStartDay, str_g_GetMonthEndDay } from '../../../common/CompanyCfg'

const cls_g_app = new appIds();

/**
 * 詳細レコードの条件と一致する別アプリの一覧へのリンクボタンを生成する
 * @module vd_g_ListViewLinkBtn -リンクボタン作成
 * @param {Object} obj_a_record -レコード情報
 * @return {*} -なし
 */
export async function vd_g_ListViewLinkBtn(obj_a_record) {

    let str_s_usercode = obj_a_record.usr_営業担当者.value[0].code;
    let str_s_stday = await str_g_GetMonthStartDay(obj_a_record.dat_対象年月bs.value);
    let str_s_endday = await str_g_GetMonthEndDay(obj_a_record.dat_対象年月bs.value);
    let obj_s_header = await kintone.app.record.getHeaderMenuSpaceElement();

    let obj_s_config = {};

    obj_s_config = [{
        "context": '当月契約売上一覧',
        "id": 'cont-list-btn',
        "clickevt": function () {
            let str_t_url = kintone.api.url('/k/').replace('.json', '');
            str_t_url += `${cls_g_app.Contract()}/`;
            str_t_url += `?query=usr_営業担当者 in ("${str_s_usercode}") and day_売上計上日 >= "${str_s_stday}" and day_売上計上日 <= "${str_s_endday}"`;
            window.open(encodeURI(str_t_url), '_blank');
        }
    }, {
        "context": '当月内装売上一覧',
        "id": 'inte-list-btn',
        "clickevt": function () {
            let str_t_url = kintone.api.url('/k/').replace('.json', '');
            str_t_url += `${cls_g_app.OtherSale()}/`;
            str_t_url += `?query=usr_営業担当者 in ("${str_s_usercode}") and day_売上計上日 >= "${str_s_stday}" and day_売上計上日 <= "${str_s_endday}"`;
            window.open(encodeURI(str_t_url), '_blank');
        }
    }];

    /**要素の初期化 */
    $(".gaia-argoui-app-toolbar-statusmenu").empty();
    for (let i in obj_s_config) {
        /* ボタンの増殖防止措置 */
        if ($('#' + obj_s_config[i].id).length == 0) {
            $('.gaia-argoui-app-toolbar-statusmenu').append(await obj_s_BtnSetting(obj_s_config[i]));
        }
    }
    return;
}

/**
 * ボタンのDOMを作成する
 * @module obj_s_BtnSetting
 * @param {Object} obj_a_param -ボタンのパラメータ情報
 * @return {Object} -ボタンのDOM
 */
function obj_s_BtnSetting(obj_a_param) {

    let obj_s_elemnt = $('<button>')
        .addClass(`kintoneplugin-button-normal`)
        .attr('id', obj_a_param.id)
        .text(obj_a_param.context)
        .css({ "marginLeft": '10px', })
        .bind('click', obj_a_param.clickevt);

    return obj_s_elemnt;
}
