import { vd_g_ListViewLinkBtn } from './draw/AppLinkBtn'
import { vd_g_DisApplinkFild } from '../../common/AllFieldCtr'

(function () {
  'use strict';

  /**レコード追加画面表示時のイベント */
  kintone.events.on('app.record.index.show', function (obj_a_event) {
    console.log(kintone.app.getQuery());
  });

  /**レコード追加画面表示時のイベント */
  kintone.events.on('app.record.create.show', function (obj_a_event) {
    vd_g_DisApplinkFild();
  });

  /**レコード編集画面表示時のイベント */
  kintone.events.on('app.record.edit.show', function (obj_a_event) {
    vd_g_DisApplinkFild();
  });

  /**レコード詳細画面表示時のイベント */
  kintone.events.on('app.record.detail.show', function (obj_a_event) {
    vd_g_ListViewLinkBtn(obj_a_event.record);
    vd_g_DisApplinkFild();
  });

  /**レコード追加画面の保存成功後イベント */
  kintone.events.on('app.record.create.submit.success', async function (obj_a_event) {
  });

  /**レコード詳細画面からレコード削除時のイベント */
  kintone.events.on('app.record.detail.delete.submit', async function (obj_a_event) {
  });
})();