import { vd_g_dropItemCrat } from './draw/dropItemSet'
import { vd_g_fldHide, obj_g_clntSwitch, obj_g_trdrSwitch, vd_g_disabledParking, vd_g_disabledCurrentParking, vd_g_disabledReasonForHope } from './draw/displayChange'
import * as FIELD from './draw/fieldCfg';
import { obj_g_newReg } from './func/newReg';
import { vd_g_ProgUpdate } from './func/ProgUpdate';
import { obj_g_PrcesEvent } from './func/ProcesLink';
import { vd_g_DisApplinkFild } from '../../common/AllFieldCtr';
import { obj_g_typeVaild } from './func/typeValidate';
import { obj_g_staffNameSet } from './func/Notice'

(function () {
  'use strict';

  /**レコード一覧画面表示時のイベント */
  kintone.events.on('app.record.index.show', function (obj_a_event) {
    let obj_s_record = sessionStorage.getItem('PervProgCal');
    if (obj_s_record != null) {
      vd_g_ProgUpdate(JSON.parse(obj_s_record));
      sessionStorage.removeItem('PervProgCal');
    }
  });

  /**レコード追加画面表示時のイベント */
  kintone.events.on('app.record.create.show', function (obj_a_event) {
    let obj_s_record = obj_a_event.record;
    vd_g_fldHide('問合せ', false);
    vd_g_fldHide('業者', false);
    vd_g_fldHide('用途', false);
    vd_g_fldHide('優先度', false);
    vd_g_dropItemCrat('問合せ', obj_s_record.txt_問合せ.value, obj_a_event);
    vd_g_dropItemCrat('業者', obj_s_record.txt_業者.value, obj_a_event);
    vd_g_dropItemCrat('用途', obj_s_record.txt_用途.value, obj_a_event);
    vd_g_dropItemCrat('優先度', obj_s_record.txt_優先度.value, obj_a_event);
    vd_g_disabledParking(obj_a_event);
    vd_g_disabledCurrentParking(obj_a_event);
    vd_g_disabledReasonForHope(obj_a_event);
    obj_g_clntSwitch(obj_a_event, obj_s_record.rdo_顧客入力方式_0.value);
    obj_g_trdrSwitch(obj_a_event, obj_s_record.rdo_業者入力方式.value);
    vd_g_DisApplinkFild();

    return obj_a_event;
  });

  /**レコード編集画面表示時のイベント*/
  kintone.events.on('app.record.edit.show', function (obj_a_event) {
    let obj_s_record = obj_a_event.record;
    vd_g_fldHide('問合せ', false);
    vd_g_fldHide('業者', false);
    vd_g_fldHide('用途', false);
    vd_g_fldHide('優先度', false);
    vd_g_dropItemCrat('問合せ', obj_s_record.txt_問合せ.value, obj_a_event);
    vd_g_dropItemCrat('業者', obj_s_record.txt_業者.value, obj_a_event);
    vd_g_dropItemCrat('用途', obj_s_record.txt_用途.value, obj_a_event);
    vd_g_dropItemCrat('優先度', obj_s_record.txt_優先度.value, obj_a_event);
    vd_g_disabledParking(obj_a_event);
    vd_g_disabledCurrentParking(obj_a_event);
    vd_g_disabledReasonForHope(obj_a_event);
    obj_g_clntSwitch(obj_a_event, obj_s_record.rdo_顧客入力方式_0.value);
    obj_g_trdrSwitch(obj_a_event, obj_s_record.rdo_業者入力方式.value);
    vd_g_DisApplinkFild();
    return obj_a_event;
  });

  kintone.events.on('app.record.detail.show', function (obj_a_event) {
    let obj_s_record = obj_a_event.record;
    vd_g_fldHide('問合せ', true);
    vd_g_fldHide('業者', true);
    vd_g_fldHide('用途', true);
    vd_g_fldHide('優先度', true);
    vd_g_dropItemCrat('問合せ', obj_s_record.txt_問合せ.value, obj_a_event);
    vd_g_dropItemCrat('業者', obj_s_record.txt_業者.value, obj_a_event);
    vd_g_dropItemCrat('用途', obj_s_record.txt_用途.value, obj_a_event);
    vd_g_dropItemCrat('優先度', obj_s_record.txt_優先度.value, obj_a_event);
    obj_g_clntSwitch(obj_a_event, obj_s_record.rdo_顧客入力方式_0.value);
    obj_g_trdrSwitch(obj_a_event, obj_s_record.rdo_業者入力方式.value);
    vd_g_DisApplinkFild();

    return obj_a_event;
  });

  kintone.events.on('app.record.create.submit', async function (obj_a_event) {

    obj_a_event = await obj_g_typeVaild(obj_a_event, 'rdo_顧客入力方式_0', 'rdo_会社種別', 'txt_御会社名_N');
    obj_a_event = await obj_g_typeVaild(obj_a_event, 'rdo_業者入力方式', 'rdo_業者種別', 'txt_業者名_N');
    obj_a_event = await obj_g_staffNameSet(obj_a_event);
    return obj_a_event;
  });

  kintone.events.on('app.record.edit.submit', async function (obj_a_event) {

    obj_a_event = await obj_g_typeVaild(obj_a_event, 'rdo_顧客入力方式_0', 'rdo_会社種別', 'txt_御会社名_N');
    obj_a_event = await obj_g_typeVaild(obj_a_event, 'rdo_業者入力方式', 'rdo_業者種別', 'txt_業者名_N');
    obj_a_event = await obj_g_staffNameSet(obj_a_event);

    return obj_a_event;
  });

  /**顧客・業者別の新規登録処理 */
  let obj_s_event = [
    'app.record.create.submit.success',
    'app.record.edit.submit.success'
  ]
  kintone.events.on(obj_s_event, async function (obj_a_event) {

    let obj_s_record = obj_a_event.record;
    let str_s_searchkey;
    let obj_s_await;
    /**
     * 顧客＝新規入力の場合、顧客マスタに情報登録後ルックアップにデータセット 
     */
    if (obj_s_record.rdo_顧客入力方式_0.value === FIELD.SWITCHING) {
      str_s_searchkey = '顧客';
      obj_s_await = await obj_g_newReg(obj_a_event, str_s_searchkey);
    }

    /**
     * 業者＝新規入力の場合、業者マスタに情報登録後ルックアップにセット 
     */
    if (obj_s_record.rdo_業者入力方式.value === FIELD.SWITCHING) {
      str_s_searchkey = '業者';
      obj_s_await = await obj_g_newReg(obj_a_event, str_s_searchkey);
    }
    /**内見数集計の更新 */
    obj_s_await = await vd_g_ProgUpdate(obj_a_event.record);

    return obj_a_event;
  });

  /**レコード一覧画面からレコード削除時のイベント */
  kintone.events.on('app.record.index.delete.submit', async function (obj_a_event) {
    /**削除後の再集計情報を設定する */
    sessionStorage.setItem('PervProgCal', JSON.stringify(obj_a_event.record));
  });

  /**レコード詳細画面からレコード削除時のイベント */
  kintone.events.on('app.record.detail.delete.submit', async function (obj_a_event) {
    /**削除後の再集計情報を設定する */
    sessionStorage.setItem('PervProgCal', JSON.stringify(obj_a_event.record));
  });

  /**プロセス管理のアクションイベント */
  kintone.events.on('app.record.detail.process.proceed', function (obj_a_event) {
    return obj_g_PrcesEvent(obj_a_event);
  });

  /**顧客入力ラジオボタンの入力フィールド切替 */
  let obj_s_clntevnt = [
    'app.record.create.change.rdo_顧客入力方式_0',
    'app.record.edit.change.rdo_顧客入力方式_0',
  ]
  kintone.events.on(obj_s_clntevnt, function (obj_a_event) {
    let obj_s_record = obj_a_event.record;
    obj_g_clntSwitch(obj_a_event, obj_s_record.rdo_顧客入力方式_0.value);
    return obj_a_event;
  });


  /**業者入力ラジオボタンの入力フィールド切替 */
  let obj_s_trdrevnt = [
    'app.record.create.change.rdo_業者入力方式',
    'app.record.edit.change.rdo_業者入力方式',
  ]
  kintone.events.on(obj_s_trdrevnt, function (obj_a_event) {

    let bln_s_evtdisflg = sessionStorage.getItem('EvtDisFlg');

    if (bln_s_evtdisflg != 'ON') {
      obj_g_trdrSwitch(obj_a_event, obj_a_event.record.rdo_業者入力方式.value)
        .then(() => {
          sessionStorage.removeItem('EvtDisFlg');
        });
    }
  });

  /**希望理由ドロップダウンの値別、フィールド活性・非活性切替 */
  let obj_s_changehope = [
    'app.record.create.change.drp_希望理由',
    'app.record.edit.change.drp_希望理由',
  ]
  kintone.events.on(obj_s_changehope, function (obj_a_event) {
    vd_g_disabledReasonForHope(obj_a_event);
    return obj_a_event;
  });

  /**駐車場ラジオボタンの値別、フィールド活性・非活性切替 */
  let obj_s_changepark = [
    'app.record.create.change.rdo_駐車場有無',
    'app.record.edit.change.rdo_駐車場有無',
  ]
  kintone.events.on(obj_s_changepark, function (obj_a_event) {
    vd_g_disabledParking(obj_a_event);
    return obj_a_event;
  });

  /**現状駐車場ラジオボタンの値別、フィールド活性・非活性切替 */
  let obj_s_changenowpark = [
    'app.record.create.change.rdo_現状駐車場有無',
    'app.record.edit.change.rdo_現状駐車場有無',
  ]
  kintone.events.on(obj_s_changenowpark, function (obj_a_event) {
    vd_g_disabledCurrentParking(obj_a_event);
    return obj_a_event;
  });


})();