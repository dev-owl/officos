import appIds from '../../../common/app_ids';
import * as PRCCFG from '../PrcesCfg';

const cls_g_app = new appIds();

export function obj_g_PrcesEvent(obj_a_event) {
    let obj_s_record = obj_a_event.record;
    obj_s_record.txt_ステータス.value = obj_a_event.nextStatus.value;

    switch (obj_a_event.action.value) {
        case PRCCFG.ACTION_ASSIGN:
            if (obj_s_record.usr_営業担当者.value.length == 0) {
                obj_a_event.error = '営業担当者が選定されていません';
            }
            break;
        case PRCCFG.ACTION_PREVIEW:
            obj_s_record.txt_内見.value = '実施';
            break;
        case PRCCFG.ACTION_DURCONTRACT:
            break;
        default:
    }
    return obj_a_event;
}