import * as FIELD from '../draw/fieldCfg';
import swal from 'sweetalert2';

export async function obj_g_typeVaild(obj_a_event, str_a_intypfld, str_a_tylefld, str_a_namefld) {

    let obj_s_record = obj_a_event.record;
    let str_s_errmsg;

    /**入力方式が情報照会であれば処理実行しない */
    if (obj_s_record[str_a_intypfld].value != FIELD.SWITCHING) {
        return obj_a_event;
    }

    if (obj_s_record[str_a_tylefld].value == '個人') {
        obj_s_record[str_a_namefld].value = '個人';
    }
    else if (obj_s_record[str_a_tylefld].value == '会社') {
        let str_t_namefldrplc = str_a_namefld.replace('txt_', '');
        let str_t_typfldrplc = str_a_tylefld.replace('rdo_', '');
        if (obj_s_record[str_a_namefld].value == undefined) {
            /**アラート */
            let str_t_swaltext = `<BR>`;
            str_t_swaltext += `<B>「${str_t_namefldrplc}」が未入力</B>です。<BR>`;
            str_t_swaltext += `「${str_t_namefldrplc}」は確認中ですか？`;
            let obj_t_resp = await swal.fire({
                "icon": 'question',
                "html": str_t_swaltext,
                showCancelButton: true,
                "confirmButtonText": 'はい',
                "cancelButtonText": 'いいえ'
            });
            /**ユーザー選択で更新 */
            if (obj_t_resp.isConfirmed) {
                obj_s_record[str_a_namefld].value = '確認中';
            }
            else {
                obj_s_record[str_a_intypfld].error = '全ての情報が「不明」または「不要」な場合は「情報照会」を選択してください';
                obj_s_record[str_a_tylefld].error = '「個人」であれば「個人」を選択してください';
                obj_s_record[str_a_namefld].error = `「${str_t_typfldrplc}」が「会社」の場合は入力してください`;
                str_s_errmsg = `・「${str_t_typfldrplc}」が「個人」であれば「${str_t_typfldrplc}」を「個人」に\n`;
                str_s_errmsg += ` 「会社」であれば「${str_t_namefldrplc}」を入力するか「${str_a_intypfld.replace('rdo_', '')}」を「情報照会」にしてください\n`;
            }
        }
    }

    /**エラーメッセージ出力判定 */
    if (str_s_errmsg != undefined) {
        if (obj_a_event.error == undefined) {
            obj_a_event.error = str_s_errmsg;
        }
        else {
            obj_a_event.error += str_s_errmsg;
        }
    }

    return obj_a_event;
}