import { vd_g_putMonthProg, vd_g_putYearProg, num_g_CalcMatterGuide, obj_g_getMonthRecInfo, num_g_getYearRecID } from '../../../common/LinkForecast';
import { str_g_GetMonthStartDay, str_g_GetYearStartDay } from '../../../common/CompanyCfg'
import appIds from '../../../common/app_ids'

const cls_g_app = new appIds();

/**
 * 実績情報の更新
 * @module vd_g_ProgUpdate
 * @param {Object} obj_a_record -レコード情報
 * @return {} -なし
 */
export async function vd_g_ProgUpdate(obj_a_record) {

    let str_s_stdate = str_g_GetYearStartDay(obj_a_record.day_問合せ日.value);

    for (let i = 0; i < obj_a_record.usr_営業担当者.value.length; i++) {

        let str_t_usercode = obj_a_record.usr_営業担当者.value[i].code;

        /**
         * 年間集計の登録
         */
        /**対象年度、営業担当のレコードIDを取得 */
        let num_t_targetid = await num_g_getYearRecID(str_s_stdate.substr(0, 4), str_t_usercode);
        if (num_t_targetid == 0) {
            alert('年間レコードが見つかりません');
            continue;
        }
        let obj_t_aggregation = await num_g_CalcMatterGuide(str_t_usercode, obj_a_record.day_問合せ日.value, true);
        let obj_t_param = {
            "num_案内件数年合計": {
                "value": obj_t_aggregation.totalcnt
            }
        }
        let obj_t_await = await vd_g_putYearProg(num_t_targetid, obj_t_param);

        /**
         * 月間集計の登録
         */
        /**対象年度、営業担当のレコードIDとルックアップ取得 */
        str_s_stdate = str_g_GetMonthStartDay(obj_a_record.day_問合せ日.value);
        let obj_t_monthrec = await obj_g_getMonthRecInfo(str_s_stdate, str_t_usercode);
        num_t_targetid = obj_t_monthrec.recid
        if (num_t_targetid == 0) {
            alert('月間レコードが見つかりません');
            continue;
        }
        obj_t_aggregation = await num_g_CalcMatterGuide(str_t_usercode, obj_a_record.day_問合せ日.value, false);
        obj_t_param = {
            "num_案内件数月合計": {
                "value": obj_t_aggregation.totalcnt
            },
            "lok_予算情報取得": {
                "value": obj_t_monthrec.lok_予算情報取得
            }
        }
        obj_t_await = await vd_g_putMonthProg(num_t_targetid, obj_t_param);
    }
    return;
}