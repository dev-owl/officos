export async function obj_g_staffNameSet(obj_a_evt) {

    let str_s_staff = '';
    let obj_s_staffrec = obj_a_evt.record.usr_営業担当者.value;

    for (let i = 0; i < obj_s_staffrec.length; i++) {
        str_s_staff += ',' + obj_s_staffrec[i].name;
    }
    obj_a_evt.record.txt_通知用営業担当.value = str_s_staff.substring(1, 100);
    return obj_a_evt;
}