import appIds from '../../../common/app_ids'

const cls_g_app = new appIds();

/**
 * 各マスタ(顧客・業者)の連携登録処理
 * @module obj_g_newReg 
 * @param {object} obj_a_event - イベント情報
 * @param {string} str_a_switchkey - 処理切替用(顧客or業者)
 */
export async function obj_g_newReg(obj_a_event, str_a_switchkey) {

    let obj_s_parm;
    let bln_s_switch = str_a_switchkey === '顧客' ? true : false;

    /**
     * マスタへ新規登録(true=顧客,false=業者)
     */
    if (bln_s_switch) {
        obj_s_parm = await obj_s_ClntPostParmCre(obj_a_event);
    } else {
        obj_s_parm = await obj_s_TrdrPostParmCre(obj_a_event);
    }
    let obj_s_mstrpostresp = await kintone.api(kintone.api.url('/k/v1/record', true), 'POST', obj_s_parm)
        .catch(() => {
            let str_s_messge = `新規入力で【${str_a_switchkey}名】が入力されていないため、\nマスタへの登録は実施していません。`;
            str_s_messge += `\n${str_a_switchkey}情報をマスタへ登録する際は、\nこのレコードの編集時に【${str_a_switchkey}名】を入力してください。`;
            str_s_messge += `\nこのレコードの編集時に【${str_a_switchkey}名】を入力してください。`;
            alert(str_s_messge);
            return 'error';
        });
    /**登録エラー時には処理を中止する */
    if (obj_s_mstrpostresp == 'error') {
        return;
    }

    /**
     * マスタ登録情報を取得(true=顧客,false=業者)
     */
    if (bln_s_switch) {
        obj_s_parm = await obj_s_ClntGetparmCre(obj_s_mstrpostresp.id);
    } else {
        obj_s_parm = await obj_s_TrdrGetparmCre(obj_s_mstrpostresp.id);
    }
    let obj_s_mstrgetresp = await kintone.api(kintone.api.url('/k/v1/record', true), 'GET', obj_s_parm);

    /**
     * マスタ登録情報を取得し、検索用フィールドへ値をセット(true=顧客,false=業者)
     */

    if (bln_s_switch) {
        obj_s_parm = await obj_s_ClntSetSearchParmCre(obj_s_mstrgetresp);
    } else {
        obj_s_parm = await obj_s_TrdrSetSearchParmCre(obj_s_mstrgetresp);
    }
    let obj_s_setput = await kintone.api(kintone.api.url('/k/v1/record', true), 'PUT', obj_s_parm);

    obj_s_parm = await kintone.api(kintone.api.url('/k/v1/record', true), 'GET', obj_s_parm);

    /**
     * ルックアップを更新(true=顧客,false=業者)
     */
    if (bln_s_switch) {
        obj_s_parm = await obj_s_MatrPutClntParmCre(obj_a_event, obj_s_parm);
    } else {
        obj_s_parm = await obj_s_MatrPutTrdrParmCre(obj_a_event, obj_s_parm);
    }
    let obj_s_matrputresp = await kintone.api(kintone.api.url('/k/v1/record', true), 'PUT', obj_s_parm)

    return obj_a_event;
}

/**
 * 顧客情報新規作成用のパラメータ作成
 * @module obj_s_ClntPostParmCre - POST用パラメーター作成
 * @param {object} obj_a_event - イベント情報
 * @return {Object} - リクエストパラメータ
 */
function obj_s_ClntPostParmCre(obj_a_event) {
    let obj_s_record = obj_a_event.record;
    let obj_s_parm;
    obj_s_parm = {
        "app": cls_g_app.Customer(),
        "record": {
            "txt_顧客名": {
                "value": obj_s_record['txt_御会社名_N'].value
            },
            "txt_顧客名かな": {
                "value": obj_s_record['txt_御会社名かな_1'].value
            },
            "txt_担当者名": {
                "value": obj_s_record['txt_御担当者名_0'].value
            },
            "txt_担当者名かな": {
                "value": obj_s_record['txt_御担当者名かな'].value
            },
            "lnk_TEL": {
                "value": obj_s_record['txt_TEL_N'].value
            },
            "lnk_携帯電話": {
                "value": obj_s_record['txt_携帯_N'].value
            },
            "lnk_FAX": {
                "value": obj_s_record['txt_FAX_N'].value
            },
            "lnk_MAIL": {
                "value": obj_s_record['txt_MAIL_N'].value
            },
        }
    };
    return obj_s_parm;
}

/**
 * 業者情報新規作成用のパラメータ作成
 * @module obj_s_TrdrPostParmCre - POST用のパラメーター作成
 * @param {object} obj_a_event - イベント情報
 * @return {Object} - リクエストパラメータ
 */
function obj_s_TrdrPostParmCre(obj_a_event) {
    let obj_s_record = obj_a_event.record;
    let obj_s_parm;
    obj_s_parm = {
        "app": cls_g_app.Trader(),
        "record": {
            "txt_業者名": {
                "value": obj_s_record['txt_業者名_N'].value
            },
            "txt_業者名かな": {
                "value": obj_s_record['txt_業者名かな_N'].value
            },
            "txt_支店部署": {
                "value": obj_s_record['txt_支店部署_N'].value
            },
            "txt_担当者名": {
                "value": obj_s_record['txt_業者担当者_N'].value
            },
            "txt_担当者名かな": {
                "value": obj_s_record['txt_業者担当者名かな_N'].value
            },
            "lnk_TEL": {
                "value": obj_s_record['txt_業者TEL_N'].value
            },
            "lnk_携帯電話": {
                "value": obj_s_record['txt_業者携帯_N'].value
            },
            "lnk_FAX": {
                "value": obj_s_record['txt_業者FAX_N'].value
            },
            "lnk_MAIL": {
                "value": obj_s_record['txt_業者MAIL_N'].value
            },
        }
    };
    return obj_s_parm;
}

/**
 * 顧客マスタ登録情報を取得するためのパラメータ作成
 * @module obj_s_ClntGetparmCre - GET用のパラメーター作成
 * @param {object} obj_a_respId - 対象ID
 * @return {object} - リクエストパラメータ
 */
function obj_s_ClntGetparmCre(obj_a_respId) {
    let obj_s_rqparm;
    obj_s_rqparm = {
        "app": cls_g_app.Customer(),
        "id": obj_a_respId,
    };

    return obj_s_rqparm;
}

/**
 * 業者マスタ登録情報を取得するためのパラメータ作成
 * @module obj_s_TrdrGetparmCre - GET用のパラメーター作成
 * @param {object} obj_a_respId - 対象ID
 * @return {object} - リクエストパラメータ
 */
function obj_s_TrdrGetparmCre(obj_a_respId) {
    let obj_s_rqparm;
    obj_s_rqparm = {
        "app": cls_g_app.Trader(),
        "id": obj_a_respId,
    };

    return obj_s_rqparm;
}

/**
 * 案件管理アプリ顧客ルックアップ更新用パラメータ作成
 * @module obj_s_MatrPutClntParmCre - PUT用のパラメーター作成
 * @param {object} obj_a_event - イベント情報
 * @param {object} obj_a_resp - 取得データ
 * @return {Object} - リクエストパラメータ
 */
function obj_s_MatrPutClntParmCre(obj_a_event, obj_a_resp) {
    let obj_s_parm;
    obj_s_parm = {
        "app": cls_g_app.Matter(),
        "id": obj_a_event.recordId,
        "record": {
            "lok_御会社名": {
                "value": obj_a_resp.record.txt_顧客検索用.value
            },
            "rdo_顧客入力方式_0": {
                "value": "顧客情報照会"
            },
            "txt_御会社名_N": {
                "value": ""
            },
            "txt_御会社名かな_1": {
                "value": ""
            },
            "txt_御担当者名_0": {
                "value": ""
            },
            "txt_御担当者名かな": {
                "value": ""
            },
            "txt_TEL_N": {
                "value": ""
            },
            "txt_携帯_N": {
                "value": ""
            },
            "txt_FAX_N": {
                "value": ""
            },
            "txt_MAIL_N": {
                "value": ""
            },
        }
    };

    return obj_s_parm;
}

/**
 * 案件管理アプリ業者ルックアップ更新用パラメータ作成
 * @module obj_s_MatrPutTrdrParmCre - PUT用のパラメーター作成
 * @param {object} obj_a_event - イベント情報
 * @param {object} obj_a_resp - 取得データ
 * @return {Object} - リクエストパラメータ
 */
function obj_s_MatrPutTrdrParmCre(obj_a_event, obj_a_resp) {
    let obj_s_parm;
    obj_s_parm = {
        "app": cls_g_app.Matter(),
        "id": obj_a_event.recordId,
        "record": {
            "lok_業者名": {
                "value": obj_a_resp.record.txt_業者検索用.value
            },
            "rdo_業者入力方式": {
                "value": "業者情報照会"
            },
            "txt_業者名_N": {
                "value": ""
            },
            "txt_業者名かな_N": {
                "value": ""
            },
            "txt_支店部署_N": {
                "value": ""
            },
            "txt_業者担当者_N": {
                "value": ""
            },
            "txt_業者担当者名かな_N": {
                "value": ""
            },
            "txt_業者TEL_N": {
                "value": ""
            },
            "txt_業者携帯_N": {
                "value": ""
            },
            "txt_業者FAX_N": {
                "value": ""
            },
            "txt_業者MAIL_N": {
                "value": ""
            },
        }
    };

    return obj_s_parm;
}

/**
 * 顧客マスタ内、検索用フィールドの更新用パラメーター作成
 * @module obj_s_ClntSetSearchParmCre 
 * @param {object} obj_s_mstrgetresp - 取得データ
 * @returns {Object} - リクエストパラメータ
 */
function obj_s_ClntSetSearchParmCre(obj_s_mstrgetresp) {
    let obj_s_parm = {
        "app": cls_g_app.Customer(),
        "id": obj_s_mstrgetresp.record.$id.value,
        "record": {
            "txt_顧客検索用": {
                "value": obj_s_mstrgetresp.record.txt_顧客名.value + "(ID:" + obj_s_mstrgetresp.record.rno_顧客番号.value + ")"
            },
        }
    };
    return obj_s_parm;
}

/**
 * 業者マスタ内、検索用フィールドの更新用パラメーター作成
 * @module obj_s_TrdrSetSearchParmCre 
 * @param {object} obj_s_mstrgetresp - 取得データ
 * @returns {Object} - リクエストパラメータ
 */
function obj_s_TrdrSetSearchParmCre(obj_s_mstrgetresp) {
    let obj_s_parm = {
        "app": cls_g_app.Trader(),
        "id": obj_s_mstrgetresp.record.$id.value,
        "record": {
            "txt_業者検索用": {
                "value": obj_s_mstrgetresp.record.txt_業者名.value + "(ID:" + obj_s_mstrgetresp.record.rno_業者番号.value + ")"
            },
        }
    };
    return obj_s_parm;
}