import str_g_FidCfg from './fieldCfg'
import swal from 'sweetalert2';

/**
 * テキストフィールド、ドロップダウン、ラジオボタンの表示切替
 * @module vd_g_fldHide -フィールドの非表示
 * @param {String} str_a_class -対象フィールド名
 * @param {boolean} bln_a_resp -表示/非表示切替
 */
export function vd_g_fldHide(str_a_class, bln_a_resp) {
    let str_s_fldname = str_a_class;
    let obj_s_getspace;

    /**各区分の設定値を取得 */
    let obj_s_filddata = str_g_FidCfg[str_s_fldname];
    /**非表示対象のフィールドコードを取得 */
    let str_s_fldcode = obj_s_filddata.valreffld;

    /**詳細画面(bln_a_resp = true)の時 */
    if (bln_a_resp) {
        /**ドロップダウンを非表示 */
        obj_s_getspace = kintone.app.record.getSpaceElement(obj_s_filddata.refelem);
        obj_s_getspace.parentNode.style.display = 'none';
        /**ラジオボタンを非表示 */
        kintone.app.record.setFieldShown('rdo_顧客入力方式_0', !bln_a_resp);
        kintone.app.record.setFieldShown('rdo_業者入力方式', !bln_a_resp);
    }
    kintone.app.record.setFieldShown(str_s_fldcode, bln_a_resp);
}

/**
 * 顧客入力方式の、イベント毎に選択値(既存or新規)でフィールド切り替えをする
 * @module obj_g_clntSwitch - 特定の値が変更された場合のみ、フィールドを切り替え
 * @param obj_a_event - イベントの情報
 * @param str_a_prop - 顧客入力方式の値
 * @return {object} - イベント情報
 */
export async function obj_g_clntSwitch(obj_a_event, str_a_prop) {
    let obj_s_record = obj_a_event.record;
    let bln_s_switch;
    let str_s_chktarget;
    let str_s_oldval;

    if (str_a_prop === '顧客情報照会') {
        bln_s_switch = true;
        str_s_chktarget = obj_s_record.txt_御会社名_N.value;
        str_s_oldval = '新規入力';
    }
    else {
        bln_s_switch = false;
        str_s_chktarget = obj_s_record.lok_御会社名.value;
        str_s_oldval = '顧客情報照会';
    }

    /**イベント毎に表示切替 */
    switch (obj_a_event.type) {
        case 'app.record.create.show':
            vd_s_clntNewFieldShow(obj_a_event, false);
            vd_s_clntExistFieldShow(obj_a_event, true);
            break;
        case 'app.record.create.change.rdo_顧客入力方式_0':
        case 'app.record.edit.change.rdo_顧客入力方式_0':
            /**アラートを出力する */
            let bln_s_chkres = await bin_s_ChgAlert(str_a_prop, str_s_chktarget);
            if (bln_s_chkres == false) {
                let obj_t_await = await kintone.app.record.get();
                obj_t_await.record.rdo_顧客入力方式_0.value = str_s_oldval;
                /**イベント停止フラグをONにする */
                sessionStorage.setItem('EvtDisFlg', 'ON');
                obj_t_await = await kintone.app.record.set(obj_t_await);
                return;
            }
            vd_s_clntValClear(bln_s_switch);
        default:
            vd_s_clntNewFieldShow(obj_a_event, !bln_s_switch);
            vd_s_clntExistFieldShow(obj_a_event, bln_s_switch);
            break;
    }
    return obj_a_event;
}

/**
 * 業者入力方式の、選択された値(既存or新規)によってフィールド切り替え
 * @module obj_g_trdrSwitch - 特定の値が変更された場合のみ、フィールドを切り替え
 * @param obj_a_event - イベントの情報
 * @param str_a_prop - 業者入力方式の値
 */
export async function obj_g_trdrSwitch(obj_a_event, str_a_prop) {

    let obj_s_record = obj_a_event.record;
    let bln_s_switch;
    let str_s_chktarget;
    let str_s_oldval;

    if (str_a_prop === '業者情報照会') {
        bln_s_switch = true;
        str_s_chktarget = obj_s_record.txt_業者名_N.value;
        str_s_oldval = '新規入力';
    }
    else {
        bln_s_switch = false;
        str_s_chktarget = obj_s_record.lok_業者名.value;
        str_s_oldval = '業者情報照会';
    }

    /**イベント毎に表示切替 */
    switch (obj_a_event.type) {
        case "app.record.create.show":
            vd_s_trdrNewFieldShow(obj_a_event, false);
            vd_s_trdrExistFieldShow(obj_a_event, true);
            break;
        case 'app.record.create.change.rdo_業者入力方式':
        case 'app.record.edit.change.rdo_業者入力方式':
            /**アラートを出力する */
            let bln_s_chkres = await bin_s_ChgAlert(str_a_prop, str_s_chktarget);
            if (bln_s_chkres == false) {
                let obj_t_await = await kintone.app.record.get();
                obj_t_await.record.rdo_業者入力方式.value = str_s_oldval;
                /**イベント停止フラグをONにする */
                sessionStorage.setItem('EvtDisFlg', 'ON');
                obj_t_await = await kintone.app.record.set(obj_t_await);
                return;
            }
            vd_s_trdrValClear(bln_s_switch);
        default:
            vd_s_trdrNewFieldShow(obj_a_event, !bln_s_switch);
            vd_s_trdrExistFieldShow(obj_a_event, bln_s_switch);
            break;
    }
    return obj_a_event;
}

/**
 * フィールドを活性/非活性を切り替える(駐車場=有)
 * @module vd_g_disabledParking - 特定の値が変更された場合のみ、フィールドを活性化
 * @param {object} obj_a_event - イベント情報
 */
export function vd_g_disabledParking(obj_a_event) {
    let obj_s_record = obj_a_event.record;
    let bln_s_truth;
    switch (obj_a_event.type) {
        case 'app.record.create.show':
            bln_s_truth = true
            break;
        case 'app.record.edit.show':
            bln_s_truth = obj_s_record.rdo_駐車場有無.value === '有' ? false : true;
            break;
        case 'app.record.create.change.rdo_駐車場有無':
        case 'app.record.edit.change.rdo_駐車場有無':
            let str_t_chengeval = obj_a_event.changes.field.value;
            bln_s_truth = str_t_chengeval === '有' ? false : true;
            if (bln_s_truth) {
                obj_s_record.txt_台数.value = '';
                obj_s_record.txt_車種.value = '';
                obj_s_record.drp_場所.value = '';
            }
            break;
    }
    obj_s_record.txt_台数.disabled = bln_s_truth;
    obj_s_record.txt_車種.disabled = bln_s_truth;
    obj_s_record.drp_場所.disabled = bln_s_truth;

    return obj_a_event;
}

/**
 * フィールドを活性/非活性を切り替える(現状駐車場=有)
 * @module vd_g_disabledCurrentParking - 特定の値が変更された場合のみ、フィールドを活性化
 * @param {object} obj_a_event - イベント情報
 */
export function vd_g_disabledCurrentParking(obj_a_event) {
    let obj_s_record = obj_a_event.record;
    let bln_s_truth;
    switch (obj_a_event.type) {
        case 'app.record.create.show':
            bln_s_truth = true
            break;
        case 'app.record.edit.show':
            bln_s_truth = obj_s_record.rdo_現状駐車場有無.value === '有' ? false : true;
            break;
        case 'app.record.create.change.rdo_現状駐車場有無':
        case 'app.record.edit.change.rdo_現状駐車場有無':
            let str_t_chengeval = obj_a_event.changes.field.value;
            bln_s_truth = str_t_chengeval === '有' ? false : true;
            if (bln_s_truth) {
                obj_s_record.txt_現状台数.value = '';
                obj_s_record.txt_現状車種.value = '';
                obj_s_record.drp_現状場所.value = '';
            }
            break;
    }
    obj_s_record.txt_現状台数.disabled = bln_s_truth;
    obj_s_record.txt_現状車種.disabled = bln_s_truth;
    obj_s_record.drp_現状場所.disabled = bln_s_truth;

    return obj_a_event;
}

/**
 * フィールドを活性/非活性を切り替える(希望理由=移転)
 * @module vd_g_disabledReasonForHope - 特定の値が変更された場合のみ、フィールドを活性化
 * @param {object} obj_a_event - イベント情報
 */
export function vd_g_disabledReasonForHope(obj_a_event) {
    let obj_s_record = obj_a_event.record;
    let bln_s_truth;
    switch (obj_a_event.type) {
        case 'app.record.create.show':
            bln_s_truth = true
            break;
        case 'app.record.edit.show':
            bln_s_truth = obj_s_record.drp_希望理由.value === '移転' ? false : true;
            break;
        case 'app.record.create.change.drp_希望理由':
        case 'app.record.edit.change.drp_希望理由':
            let str_t_chengeval = obj_a_event.changes.field.value;
            bln_s_truth = str_t_chengeval === '移転' ? false : true;
            if (bln_s_truth) {
                obj_s_record.txt_解除予告.value = '';
            }
            break;
    }
    obj_s_record.txt_解除予告.disabled = bln_s_truth;

    return obj_a_event;
}

/**
 * 顧客情報入力のフィールド切り替え処理(既存用)
 * @module vd_s_clntExistFieldShow - 既存用フィールドの表示設定
 * @param {object} obj_a_event - イベント情報
 * @param {boolean} bln_a_resp - フィールド表示の切替え
 * @return {} - なし
 */
function vd_s_clntExistFieldShow(obj_a_event, bln_a_resp) {
    kintone.app.record.setFieldShown('lok_御会社名', bln_a_resp);
    kintone.app.record.setFieldShown('txt_御担当者名', bln_a_resp);
    kintone.app.record.setFieldShown('lnk_TEL', bln_a_resp);
    kintone.app.record.setFieldShown('lnk_携帯', bln_a_resp);
    kintone.app.record.setFieldShown('lnk_FAX', bln_a_resp);
    kintone.app.record.setFieldShown('lnk_MAIL', bln_a_resp);
}

/**
 * 顧客情報入力のフィールド切り替え処理(新規入力用)
 * @module vd_s_clntNewFieldShow - 新規入力用フィールドの表示設定
 * @param {object} obj_a_event - イベント情報
 * @param {boolean} bln_a_resp - フィールド表示の切替え
 * @return {} - なし
 */
function vd_s_clntNewFieldShow(obj_a_event, bln_a_resp) {
    let obj_s_record = obj_a_event.record;
    kintone.app.record.setFieldShown('rdo_会社種別', bln_a_resp);
    kintone.app.record.setFieldShown('txt_御会社名_N', bln_a_resp);
    kintone.app.record.setFieldShown('txt_御会社名かな_1', bln_a_resp);
    kintone.app.record.setFieldShown('txt_御担当者名_0', bln_a_resp);
    kintone.app.record.setFieldShown('txt_御担当者名かな', bln_a_resp);
    kintone.app.record.setFieldShown('txt_TEL_N', bln_a_resp);
    kintone.app.record.setFieldShown('txt_携帯_N', bln_a_resp);
    kintone.app.record.setFieldShown('txt_FAX_N', bln_a_resp);
    kintone.app.record.setFieldShown('txt_MAIL_N', bln_a_resp);
}

/**
 * 顧客情報入力方式の値別、フィールド値クリア処理
 * @module vd_s_clntValClear - 入力方式の値別のフィールド値クリア処理
 * @param {object} obj_a_record - イベントのレコード情報
 * @param {boolean} bln_a_resp - フィールドのクリア切替え(true = 既存, false = 新規) 
 * @return {} - なし
 */
async function vd_s_clntValClear(bln_a_resp) {

    let obj_t_get = kintone.app.record.get();
    let obj_a_record = obj_t_get.record;

    if (bln_a_resp) {
        obj_a_record.txt_御会社名_N.value = '';
        obj_a_record.txt_御会社名かな_1.value = '';
        obj_a_record.txt_御担当者名_0.value = '';
        obj_a_record.txt_御担当者名かな.value = '';
        obj_a_record.txt_TEL_N.value = '';
        obj_a_record.txt_携帯_N.value = '';
        obj_a_record.txt_FAX_N.value = '';
        obj_a_record.txt_MAIL_N.value = '';
    } else {
        obj_a_record['lok_御会社名'].lookup = 'CLEAR';
        obj_a_record.txt_御担当者名.value = '';
        obj_a_record.lnk_TEL.value = '';
        obj_a_record.lnk_携帯.value = '';
        obj_a_record.lnk_FAX.value = '';
        obj_a_record.lnk_MAIL.value = '';
    }
    kintone.app.record.set(obj_t_get);
}

/**
 * 業者情報入力のフィールド切り替え処理(既存)
 * @module vd_s_trdrExistFieldShow - 既存用フィールドの表示設定
 * @param {object} obj_a_event - イベント情報
 * @param {boolean} bln_a_resp - フィールド表示の切替え
 * @return {} - なし
 */
function vd_s_trdrExistFieldShow(obj_a_event, bln_a_resp) {
    kintone.app.record.setFieldShown('lok_業者名', bln_a_resp);
    kintone.app.record.setFieldShown('txt_支店部署', bln_a_resp);
    kintone.app.record.setFieldShown('txt_業者担当者', bln_a_resp);
    kintone.app.record.setFieldShown('lnk_業者TEL', bln_a_resp);
    kintone.app.record.setFieldShown('lnk_業者携帯', bln_a_resp);
    kintone.app.record.setFieldShown('lnk_業者FAX', bln_a_resp);
    kintone.app.record.setFieldShown('lnk_業者MAIL', bln_a_resp);
}

/**
 * 業者情報入力のフィールド切り替え処理(新規入力)
 * @module vd_s_trdrNewFieldShow - 新規入力用フィールドの表示設定
 * @param {object} obj_a_event - イベント情報
 * @param {boolean} bln_a_resp - フィールド表示の切替え
 * @return {} - なし
 */
function vd_s_trdrNewFieldShow(obj_a_event, bln_a_resp) {
    let obj_s_record = obj_a_event.record;
    kintone.app.record.setFieldShown('rdo_業者種別', bln_a_resp);
    kintone.app.record.setFieldShown('txt_業者名_N', bln_a_resp);
    kintone.app.record.setFieldShown('txt_業者名かな_N', bln_a_resp);
    kintone.app.record.setFieldShown('txt_支店部署_N', bln_a_resp);
    kintone.app.record.setFieldShown('txt_業者担当者_N', bln_a_resp);
    kintone.app.record.setFieldShown('txt_業者担当者名かな_N', bln_a_resp);
    kintone.app.record.setFieldShown('txt_業者TEL_N', bln_a_resp);
    kintone.app.record.setFieldShown('txt_業者携帯_N', bln_a_resp);
    kintone.app.record.setFieldShown('txt_業者FAX_N', bln_a_resp);
    kintone.app.record.setFieldShown('txt_業者MAIL_N', bln_a_resp);
}

/**
 * 業者情報入力方式の値別、フィールド値クリア処理
 * @module vd_s_trdrValClear - 入力方式の値別のフィールド値クリア処理
 * @param {object} obj_a_record - イベントのレコード情報
 * @param {boolean} bln_a_resp - フィールドのクリア切替え(true = 既存, false = 新規) 
 * @return {} - なし
 */
function vd_s_trdrValClear(bln_a_resp) {

    let obj_t_get = kintone.app.record.get();
    let obj_a_record = obj_t_get.record;

    if (bln_a_resp) {
        obj_a_record.txt_業者名_N.value = '';
        obj_a_record.txt_業者名かな_N.value = '';
        obj_a_record.txt_支店部署_N.value = '';
        obj_a_record.txt_業者担当者_N.value = '';
        obj_a_record.txt_業者担当者名かな_N.value = '';
        obj_a_record.txt_業者TEL_N.value = '';
        obj_a_record.txt_業者携帯_N.value = '';
        obj_a_record.txt_業者FAX_N.value = '';
        obj_a_record.txt_業者MAIL_N.value = '';
    } else {
        obj_a_record['lok_業者名'].lookup = 'CLEAR';
        obj_a_record.txt_支店部署.value = '';
        obj_a_record.txt_業者担当者.value = '';
        obj_a_record.lnk_業者TEL.value = '';
        obj_a_record.lnk_業者携帯.value = '';
        obj_a_record.lnk_業者FAX.value = '';
        obj_a_record.lnk_業者MAIL.value = '';
    }
    kintone.app.record.set(obj_t_get);
}

/**
 * 切替の確認アラートを出す
 * @module bin_s_ChgAlert - 入力方式の値別のフィールド値クリア処理
 * @param {String} str_a_chseval -選択値
 * @param {String} str_a_chktarget -アラート出力判断フィールド内容
 * @return -ユーザー選択結果
 */
async function bin_s_ChgAlert(str_a_chseval, str_a_chktarget) {

    /**対象のフィールドが空欄の場合はアラートを出さない */
    if (str_a_chktarget == undefined) { return };

    let bln_s_rtn = false;
    let str_s_swaltext = `${str_a_chseval}に切り替えると入力された内容が`;
    str_s_swaltext += '<BR>クリアされる部分があります。';
    str_s_swaltext += `<BR>切替を実施しますか？`;

    let obj_s_resp = await swal.fire({
        "icon": 'warning',
        "html": str_s_swaltext,
        showCancelButton: true,
        "confirmButtonText": 'はい',
        "cancelButtonText": 'いいえ'
    });

    /**ユーザー選択で更新 */
    if (obj_s_resp.isConfirmed) {
        bln_s_rtn = true;
    }

    return bln_s_rtn;

}