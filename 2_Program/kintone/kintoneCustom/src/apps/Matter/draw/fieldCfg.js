export const SWITCHING = '新規入力';
export const INQUIRY = '情報照会';
export const fieldcfg = {
    "問合せ": {
        "srchkey": "問合せ",
        "label": "問合せ",
        "refelem": "drp_Toiawase",
        "valreffld": "txt_問合せ"
    },
    "業者": {
        "srchkey": "業者",
        "label": "業者",
        "refelem": "drp_Gyosya",
        "valreffld": "txt_業者"
    },
    "用途": {
        "srchkey": "用途",
        "label": "用途",
        "refelem": "drp_Yoto",
        "valreffld": "txt_用途"
    },
    "優先度": {
        "srchkey": "優先度",
        "label": "優先度",
        "refelem": "drp_Priority",
        "valreffld": "txt_優先度"
    },
}

export default fieldcfg;