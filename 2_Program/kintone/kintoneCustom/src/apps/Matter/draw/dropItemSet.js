import appIds from '../../../common/app_ids';
import fieldcfg from './fieldCfg'
import { Dropdown } from 'kintone-ui-component/lib/Dropdown';

const cls_g_app = new appIds();

/**
 * マスタの項目をドロップダウンに反映して、テキストフィールドで値を保持させる
 * @module vd_g_dropItemCrat - マスタ抽出のドロップダウン作成
 * @param {String} str_a_class - マスタ抽出区分
 * @param {String} str_a_txtVal - ドロップダウンの値がセットされているフィールドの値
 * @param {Object} obj_a_event - イベント情報
 */
export function vd_g_dropItemCrat(str_a_class, str_a_txtVal, obj_a_event) {
    /**各区分の設定値を取得 */
    let obj_s_fielddata = fieldcfg[str_a_class];

    /**区分マスタからデータを取得し、問合せドロップダウンに値を設定する */
    let obj_s_rqbody = {
        "app": cls_g_app.ClassMaster(),                                                     /* 区分マスタのアプリID */
        "query": `drp_区分 in ("${obj_s_fielddata.srchkey}") order by num_表示順 asc`    /* 問合せデータの取得 */
    };

    kintone.api(kintone.api.url('/k/v1/records', true), 'GET', obj_s_rqbody, function (obj_a_resp) {

        /**レコード情報の置き換え */
        let obj_s_records = obj_a_resp.records;

        /** ドロップダウンの設定 */
        let str_s_items = [];
        obj_s_records.forEach(function (obj_a_record, num_a_idx) {
            let obj_t_item = { "label": obj_a_record.txt_名称.value, "value": obj_a_record.txt_名称.value };
            str_s_items.push(obj_t_item);
        });

        /**ドロップダウンElementの作成 */
        let obj_s_dropdown = new Dropdown({
            "label": obj_s_fielddata.label,
            "items": str_s_items
        });

        /**イベント毎に処理(ドロップダウンの活性/不活性)を切り替え */
        switch (obj_a_event.type) {
            case "app.record.detail.show":  /**詳細画面では値をセットし、フィールド不活性*/
                obj_s_dropdown.value = str_a_txtVal;
                obj_s_dropdown.disabled = true;
                break;
            case "app.record.edit.show":    /**編集画面では値のセットのみ */
                obj_s_dropdown.value = str_a_txtVal;
                break;
        }

        kintone.app.record.getSpaceElement(obj_s_fielddata.refelem).appendChild(obj_s_dropdown);

        /**ドロップダウンのアイテム変更時イベント */
        obj_s_dropdown.addEventListener('change', function (str_a_selitem) {
            let obj_s_record = kintone.app.record.get();

            /**マスタ情報の値を取得 */
            for (let i = 0; i < obj_s_records.length; i++) {
                if (str_a_selitem.detail.value == obj_s_records[i].txt_名称.value) {
                    obj_s_record['record'][obj_s_fielddata.valreffld]['value'] = obj_s_records[i].txt_名称.value;
                }
            }
            kintone.app.record.set(obj_s_record);
        });
    }, function (obj_a_error) {
        /**error */
        console.log(obj_a_error);
    });

}

