/**プロセス管理のステータス */
export const STATUS_RECEP = '未処理';
export const STATUS_ASSIGN = '担当割当中';
export const STATUS_PROGRESS = '進行中';
export const STATUS_PROGPREVIEW = '進行中（内見済）';
export const STATUS_DURCONTRACT = '契約作業中';
export const STATUS_CONTRACT = '契約完了';
export const STATUS_MISSED = '失注';
/**プロセス管理のアクション */
export const ACTION_REQASSIGN = '担当者割当依頼';
export const ACTION_ASSIGN = '担当者決定';
export const ACTION_PREVIEW = '内見実施';
export const ACTION_DURCONTRACT = '契約作業開始';
export const ACTION_CONTRACT = '契約完了';
export const ACTION_MISSED = '失注';
export const ACTION_INIT = '初期状態に戻す';
