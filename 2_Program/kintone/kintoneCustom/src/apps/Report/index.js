import { obj_g_clntSwitch } from './draw/displayChange';
import { vd_g_NextActivity, vd_g_PreviewUpdate } from './func/MatterLink';
import { obj_g_RepRequird } from './func/FieldChk'
import { vd_g_DisApplinkFild } from '../../common/AllFieldCtr'

(function () {
  'use strict';

  /**レコード追加画面表示時のイベント */
  kintone.events.on('app.record.create.show', function (obj_a_event) {
    let obj_s_record = obj_a_event.record;

    /**案件入力方式ラジオボタンの処理 */
    obj_g_clntSwitch(obj_a_event, obj_s_record.rdo_案件入力方式.value);
    vd_g_DisApplinkFild();
    return obj_a_event;
  });


  /**レコード編集画面表示時のイベント*/
  kintone.events.on('app.record.edit.show', function (obj_a_event) {
    let obj_s_record = obj_a_event.record;

    /**案件入力方式ラジオボタンの処理 */
    obj_g_clntSwitch(obj_a_event, obj_s_record.rdo_案件入力方式.value);
    vd_g_DisApplinkFild();
    return obj_a_event;
  });


  /**レコード詳細画面表示時のイベント*/
  kintone.events.on('app.record.detail.show', function (obj_a_event) {
    let obj_s_record = obj_a_event.record;

    /**案件入力方式ラジオボタンの処理 */
    obj_g_clntSwitch(obj_a_event, obj_s_record.rdo_案件入力方式.value);
    vd_g_DisApplinkFild();

    return obj_a_event;
  });


  /**案件入力方式ラジオボタン切り替え時のイベント(レコード追加画面) */
  kintone.events.on('app.record.create.change.rdo_案件入力方式', function (obj_a_event) {
    let obj_s_record = obj_a_event.record;

    /**案件入力方式ラジオボタンの処理 */
    obj_g_clntSwitch(obj_a_event, obj_s_record.rdo_案件入力方式.value);
    return obj_a_event;
  });


  /**案件入力方式ラジオボタン切り替え時のイベント(レコード追加画面) */
  kintone.events.on('app.record.edit.change.rdo_案件入力方式', function (obj_a_event) {
    let obj_s_record = obj_a_event.record;

    /**案件入力方式ラジオボタンの処理 */
    obj_g_clntSwitch(obj_a_event, obj_s_record.rdo_案件入力方式.value);
    return obj_a_event;
  });


  /**レコード追加画面の保存実行前イベント */
  kintone.events.on('app.record.create.submit', async function (obj_a_event) {
    return obj_g_RepRequird(obj_a_event);
  });

  /**レコード追加完了後のイベント */
  kintone.events.on('app.record.create.submit.success', async function (obj_a_event) {
    let obj_s_record = obj_a_event.record;

    /**案件入力方式ラジオボタンの処理 */
    obj_g_clntSwitch(obj_a_event, obj_s_record.rdo_案件入力方式.value);
    let obj_s_await = await vd_g_NextActivity(obj_a_event.record);
    obj_s_await = await vd_g_PreviewUpdate(obj_a_event.record);

    return obj_a_event;
  });

  /**レコード編集画面の保存実行前イベント */
  kintone.events.on('app.record.edit.submit', async function (obj_a_event) {
    return obj_g_RepRequird(obj_a_event);
  });
  /**レコード編集画面の保存成功後イベント */
  kintone.events.on('app.record.edit.submit.success', async function (obj_a_event) {
    let obj_s_record = obj_a_event.record;

    /**案件入力方式ラジオボタンの処理 */
    obj_g_clntSwitch(obj_a_event, obj_s_record.rdo_案件入力方式.value);
    let obj_s_await = await vd_g_NextActivity(obj_a_event.record);
    obj_s_await = await vd_g_PreviewUpdate(obj_a_event.record);

    return obj_a_event;
  });

})();