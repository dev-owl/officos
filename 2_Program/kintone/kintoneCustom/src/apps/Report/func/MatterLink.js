import appIds from '../../../common/app_ids'
import * as PRCCFG from '../../Matter/PrcesCfg';
import * as FIELD from '../fieldCfg'
import swal from 'sweetalert2';

const cls_g_app = new appIds();

/**
 * 
 * @param {Object} obj_a_record -レコード情報
 * @return 
 */
export async function vd_g_NextActivity(obj_a_record) {

    if (obj_a_record.rdo_案件入力方式.value != FIELD.MATTER) { return; }

    let num_s_matid = obj_a_record.num_案件ID.value;
    let dat_s_regnextact = obj_a_record.dat_次回活動予定日時.value;
    let bln_s_updateflg = false;

    let obj_s_rqparm = {
        "app": cls_g_app.Matter(),
        "id": num_s_matid
    };
    let obj_s_resp = await kintone.api(kintone.api.url('/k/v1/record', true), 'GET', obj_s_rqparm);
    let dat_s_matnextact = obj_s_resp.record.dat_次回活動予定.value;

    if (dat_s_matnextact > dat_s_regnextact) {

        let dat_t_date = new Date(dat_s_matnextact);
        let str_t_swaltext = `該当案件の次回活動予定は<BR>`;
        str_t_swaltext += `<B>${dat_t_date.getFullYear()}-${dat_t_date.getMonth() + 1}-${dat_t_date.getDate()}</B>です。<BR>`;
        str_t_swaltext += `今回登録した次回活動予定で更新しますか？`;
        obj_s_resp = await swal.fire({
            "icon": 'question',
            "html": str_t_swaltext,
            showCancelButton: true,
            "confirmButtonText": 'はい',
            "cancelButtonText": 'いいえ'
        });
        /**ユーザー選択で更新 */
        if (obj_s_resp.isConfirmed) {
            bln_s_updateflg = true;
        }
    }
    else {
        /**次回活動履歴が古ければ強制更新 */
        bln_s_updateflg = true;
    }

    /**案件管理アプリへ登録 */
    if (bln_s_updateflg) {
        obj_s_rqparm.record = {
            "dat_次回活動予定": { "value": dat_s_regnextact },
            "txt_次回行動計画": { "value": obj_a_record.txt_次回行動計画.value }
        }
        obj_s_resp = await kintone.api(kintone.api.url('/k/v1/record', true), 'PUT', obj_s_rqparm);
    }

    return;
}

/**
 * 
 * @param {Object} obj_a_record 
 */
export async function vd_g_PreviewUpdate(obj_a_record) {

    if (obj_a_record.chk_内見.value.indexOf('内見実施') !== -1) {
        let num_t_matid = obj_a_record.num_案件ID.value;

        let obj_t_rqparm = {
            "app": cls_g_app.Matter(),
            "id": num_t_matid,
            "record": { "txt_内見": { "value": '実施' } },
            "action": PRCCFG.ACTION_PREVIEW
        };
        /**ステータスはエラーありきで登録する */
        let obj_s_resp = await kintone.api(kintone.api.url('/k/v1/record/status', true), 'PUT', obj_t_rqparm)
            .then(async () => {
                let obj_s_resp = await kintone.api(kintone.api.url('/k/v1/record', true), 'PUT', obj_t_rqparm);
            })
            .catch(() => {
                return;
            });
    }
    return;
}