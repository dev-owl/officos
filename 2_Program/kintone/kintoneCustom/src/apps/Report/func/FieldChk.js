import * as FIELD from '../fieldCfg'

const cnsERRMESSAGE = '案件の営業日報登録時は必須です';

export function obj_g_RepRequird(obj_a_event) {

    let obj_s_record = obj_a_event.record;
    let str_sa_errormsg = [];
    switch (obj_s_record.rdo_案件入力方式.value) {
        /**案件管理参照選択時 */
        case FIELD.MATTER:
            let str_t_mtrsts = obj_s_record.txt_案件ステータス.value;
            let bln_t_stsok = false;
            if ((str_t_mtrsts == '契約完了') || (str_t_mtrsts == '失注')) {
                bln_t_stsok = true;
            }
            if (obj_s_record.lok_対応案件.value == undefined) {
                obj_s_record.lok_対応案件.error = cnsERRMESSAGE;
                str_sa_errormsg.push('対応案件');
            }
            if ((obj_s_record.dat_次回活動予定日時.value == undefined)
                && (bln_t_stsok == false)) {
                obj_s_record.dat_次回活動予定日時.error = cnsERRMESSAGE;
                str_sa_errormsg.push('・次回活動予定日時');
            }
            if ((obj_s_record.txt_次回行動計画.value == undefined)
                && (bln_t_stsok == false)) {
                obj_s_record.txt_次回行動計画.error = cnsERRMESSAGE;
                str_sa_errormsg.push('・次回行動計画');
            }
            break;
        /**オーナー情報参照選択時 */
        case FIELD.ORNER:
            if (obj_s_record.lok_オーナー他業者名称.value == undefined) {
                obj_s_record.lok_オーナー他業者名称.error = cnsERRMESSAGE;
                str_sa_errormsg.push('・オーナー他業者名称');
            }
            break;
        /**その他選択時 */
        case FIELD.ETC:
            if (obj_s_record.txt_その他訪問先.value == undefined) {
                obj_s_record.txt_その他訪問先.error = cnsERRMESSAGE;
                str_sa_errormsg.push('・その他訪問先');
            }
            break;
    }

    if (str_sa_errormsg.length > 0) {
        let str_t_msg = '以下の入力内容にエラーがあるため保存できません。';
        for (let i = 0; i < str_sa_errormsg.length; i++) {
            str_t_msg += `\n${str_sa_errormsg[i]}`;
        }
        obj_a_event.error = str_t_msg;
    }
    return obj_a_event;
}