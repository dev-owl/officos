import * as FIELD from '../fieldCfg'

/**
 * 案件入力方式ラジオボタンのイベント毎の表示切り替え処理
 * @module obj_g_clntSwitch - 特定の値が変更された場合のみ、フィールドを切り替え
 * @param obj_a_event - イベントの情報
 * @param str_a_prop - 案件入力方式ラジオボタンの選択値
 * @return {object} - イベント情報
 */
export function obj_g_clntSwitch(obj_a_event, str_a_prop) {
    let obj_s_record = obj_a_event.record;
    /**イベント毎の処理 */
    switch (obj_a_event.type) {
        /**レコード追加画面表示時 */
        case 'app.record.create.show':
            /**案件情報フィールド表示切替 */
            vd_s_clntDisplayMatter(obj_a_event, str_a_prop);
            /**案件情報フィールドクリア処理 */
            vd_s_clntClearMatter(obj_a_event, obj_s_record, str_a_prop)
            break;
        /**レコード編集画面表示時 */
        case 'app.record.edit.show':
            /**案件情報フィールド表示切替 */
            vd_s_clntDisplayMatter(obj_a_event, str_a_prop);
            /**案件情報フィールドクリア処理 */
            vd_s_clntClearMatter(obj_a_event, obj_s_record, str_a_prop)
            break;
        /**レコード照会画面表示時 */
        case 'app.record.detail.show':
            /**案件情報フィールド表示切替 */
            vd_s_clntDisplayMatter(obj_a_event, str_a_prop);
            /**案件情報フィールドクリア処理 */
            vd_s_clntClearMatter(obj_a_event, obj_s_record, str_a_prop)
            /**案件入力方式非表示 */
            kintone.app.record.setFieldShown('rdo_案件入力方式', false);
            break;
        /**案件入力方式ラジオボタン切り替え時(追加) */
        case 'app.record.create.change.rdo_案件入力方式':
            /**案件情報フィールド表示切替 */
            vd_s_clntDisplayMatter(obj_a_event, str_a_prop);
            /**案件情報フィールドクリア処理 */
            vd_s_clntClearMatter(obj_a_event, obj_s_record, str_a_prop)
            break;
        /**案件入力方式ラジオボタン切り替え時(編集) */
        case 'app.record.edit.change.rdo_案件入力方式':
            /**案件情報フィールド表示切替 */
            vd_s_clntDisplayMatter(obj_a_event, str_a_prop);
            /**案件情報フィールドクリア処理 */
            vd_s_clntClearMatter(obj_a_event, obj_s_record, str_a_prop)
            break;
        default:
            break;
    }
    return obj_a_event;
}

/**
 * 案件入力方式ラジオボタン選択値ごとの表示切替処理
 * @module vd_s_clntDisplayMatter - 案件情報フィールドの表示切替
 * @param obj_a_event - イベント情報
 * @param str_a_prop - 案件入力方式ラジオボタンの選択値
 * @return {} - なし
 */
function vd_s_clntDisplayMatter(obj_a_event, str_a_prop) {
    /**案件入力方式ラジオボタンの選択値毎の処理 */
    switch (str_a_prop) {
        /**案件管理参照選択時 */
        case FIELD.MATTER:
            kintone.app.record.setFieldShown('lok_対応案件', true);
            kintone.app.record.setFieldShown('rec_案件情報', true);
            kintone.app.record.setFieldShown('lok_オーナー他業者名称', false);
            kintone.app.record.setFieldShown('txt_オーナー他業者担当者', false);
            kintone.app.record.setFieldShown('txt_その他訪問先', false);
            break;
        /**オーナー情報参照選択時 */
        case FIELD.ORNER:
            kintone.app.record.setFieldShown('lok_対応案件', false);
            kintone.app.record.setFieldShown('rec_案件情報', false);
            kintone.app.record.setFieldShown('lok_オーナー他業者名称', true);
            kintone.app.record.setFieldShown('txt_オーナー他業者担当者', true);
            kintone.app.record.setFieldShown('txt_その他訪問先', false);
            break;
        /**その他選択時 */
        case FIELD.ETC:
            kintone.app.record.setFieldShown('lok_対応案件', false);
            kintone.app.record.setFieldShown('rec_案件情報', false);
            kintone.app.record.setFieldShown('lok_オーナー他業者名称', false);
            kintone.app.record.setFieldShown('txt_オーナー他業者担当者', false);
            kintone.app.record.setFieldShown('txt_その他訪問先', true);
            break;
    }
}

/**
 * 案件入力方式ラジオボタン選択値ごとの案件情報フィールドのクリア処理
 * @module vd_s_clntClearMatter - 案件情報フィールドのクリア処理
 * @param obj_a_event - イベント情報
 * @param obj_a_record - レコード情報
 * @param str_a_prop - 案件入力方式ラジオボタンの選択値
 * @return {} - なし
 */
 function vd_s_clntClearMatter(obj_a_event, obj_a_record, str_a_prop) {
    /**案件入力方式ラジオボタンの選択値毎の処理 */
    switch (str_a_prop) {
        /**案件管理参照選択時 */
        case FIELD.MATTER:
            obj_a_record['lok_オーナー他業者名称'].lookup = 'CLEAR';
            obj_a_record.txt_オーナー他業者担当者.value = '';
            obj_a_record.txt_その他訪問先.value = '';
            break;
        /**オーナー情報参照選択時 */
        case FIELD.ORNER:
            obj_a_record['lok_対応案件'].lookup = 'CLEAR';
            obj_a_record.txt_その他訪問先.value = '';
            break;
        /**その他選択時 */
        case FIELD.ETC:
            obj_a_record['lok_対応案件'].lookup = 'CLEAR';
            obj_a_record['lok_オーナー他業者名称'].lookup = 'CLEAR';
            obj_a_record.txt_オーナー他業者担当者.value = '';
            break;
    }
}