import { obj_g_recSubdivision } from '../Budget/func/recSubdivision'
import { obj_g_recDelete } from '../Budget/func/recDelete'
import { obj_g_clntSwitch } from '../Budget/draw/displayChange'
import { vd_g_DisApplinkFild } from '../../common/AllFieldCtr'

(function () {
    'use strict';

    /**レコード詳細画面表示時のイベント */
    kintone.events.on('app.record.detail.show', function (obj_a_event) {
        vd_g_DisApplinkFild();
        return obj_a_event;
    });

    /**レコード追加画面表示時のイベント */
    kintone.events.on('app.record.create.show', function (obj_a_event) {

        /**フィールド非活性処理 */
        // obj_g_clntSwitch(obj_a_event);
        vd_g_DisApplinkFild();
        return obj_a_event;
    });

    /**レコード編集画面表示時のイベント*/
    kintone.events.on('app.record.edit.show', function (obj_a_event) {

        /**フィールド非活性処理 */
        // obj_g_clntSwitch(obj_a_event);
        vd_g_DisApplinkFild();
        return obj_a_event;
    });

    /**レコード新規保存成功後のイベント */
    kintone.events.on('app.record.create.submit.success', async function (obj_a_event) {

        let obj_s_rtn = await obj_g_recSubdivision(obj_a_event)
        console.log(obj_s_rtn)
        return;
    });

    /**レコード削除時のイベント */
    kintone.events.on('app.record.index.delete.submit', async function (obj_a_event) {

        let obj_s_rtn = await obj_g_recDelete(obj_a_event);
        console.log(obj_s_rtn)
        return;
    });

})();