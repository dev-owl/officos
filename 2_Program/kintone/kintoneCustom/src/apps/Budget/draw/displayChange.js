/**
 * フィールド非活性切り替え処理
 * @module obj_g_clntSwitch - フィールドを非活性に切り替え
 * @param obj_a_event - イベントの情報
 * @return {object} - イベント情報
 */
export function obj_g_clntSwitch(obj_a_event) {
    let obj_s_record = obj_a_event.record;
    /**フィールドの非活性処理 */
    switch (obj_a_event.type) {
        /**レコード追加画面表示時 */
        case 'app.record.create.show':
            obj_s_record.num_売上年合計.disabled = true;
            obj_s_record.num_案内件数年合計.disabled = true;
            obj_s_record.num_契約件数年合計.disabled = true;
            break;
        /**レコード編集画面表示時のイベント */
        case 'app.record.edit.show':
            obj_s_record.num_売上年合計.disabled = true;
            obj_s_record.num_案内件数年合計.disabled = true;
            obj_s_record.num_契約件数年合計.disabled = true;
            obj_s_record.num_年度.disabled = true;
            obj_s_record.drp_作成開始月.disabled = true;
            obj_s_record.usr_営業担当者.disabled = true;
            break;
    }
    return obj_a_event;
}
