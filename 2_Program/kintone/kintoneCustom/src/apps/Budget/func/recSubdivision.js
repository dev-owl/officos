import appIds from '../../../common/app_ids'

const cls_g_app = new appIds();

/**
 * 作成開始月を元に月間予算入力レコードを新規登録する
 * @module obj_g_recSubdivision
 * @param {object} obj_a_event - イベント情報
 */
export function obj_g_recSubdivision(obj_a_event) {
    let obj_s_record = obj_a_event.record;
    /**
     * 月レコード登録処理の前準備(文字列を数字に変換)
     */
    let num_s_setyear = parseInt(obj_s_record.num_年度.value, 10);
    let num_s_setmonth = parseInt(obj_s_record.drp_作成開始月.value, 10);
    let str_s_setday = "01";

    let num_s_monthfirst = 0;     //上期の分数
    let num_s_monthsecond = 0;    //下期の分数

    switch (num_s_setmonth) {
        case 3:
            num_s_monthfirst = 6;
            num_s_monthsecond = 6;
            break;
        case 4:
            num_s_monthfirst = 5;
            num_s_monthsecond = 6;
            break;
        case 5:
            num_s_monthfirst = 4;
            num_s_monthsecond = 6;
            break;
        case 6:
            num_s_monthfirst = 3;
            num_s_monthsecond = 6;
           break;
        case 7:
            num_s_monthfirst = 2;
            num_s_monthsecond = 6;
            break;
        case 8:
            num_s_monthfirst = 1;
            num_s_monthsecond = 6;
            break;
        case 9:
            num_s_monthfirst = 0;
            num_s_monthsecond = 6;
            break;
        case 10:
            num_s_monthfirst = 0;
            num_s_monthsecond = 5;
            break;
        case 11:
            num_s_monthfirst = 0;
            num_s_monthsecond = 4;
            break;
        case 12:
            num_s_monthfirst = 0;
            num_s_monthsecond = 3;
            break;
        case 1:
            num_s_monthfirst = 0;
            num_s_monthsecond = 2;
            break;
        case 2:
            num_s_monthfirst = 0;
            num_s_monthsecond = 1;
            break;
    }

    /**
     * 月間予算レコード登録処理の前準備(パラメータ作成)
     */
    let str_s_parm = {
        'app': cls_g_app.Forecast(),
    };
    let obj_s_records = [];
    let obj_s_userval = obj_s_record['usr_営業担当者'].value;
    let obj_s_budgetid = obj_s_record['rno_予算管理番号'].value;
    /**
     * 月間予算レコード登録用データ作成
     * 作成開始月から2月までレコードを作成する
     */
    let num_s_contractmoney = 0;    //月間契約売上目標
    let num_s_interiormoney = 0;    //月間内装売上目標
    let num_s_matternumber = 0;     //月間案内目標件数
    let num_s_contractnumber = 0;   //月間契約目標件数
    let num_s_interiornumber = 0;   //月間内装目標件数
    let str_t_setyear;
    let str_t_setmonth;
    do{
        /**セット月数が12を超えたら値を1月にリセット、セット年数を1繰り上げる */
        if (num_s_setmonth > 12) {
            num_s_setmonth = 1;     //月数
            num_s_setyear++;        //年数
        }
        /**数字を文字に変換、月数が1桁の場合先頭に0をつける */
        str_t_setyear = String(num_s_setyear);
        str_t_setmonth = String(num_s_setmonth);
        if (str_t_setmonth.length == 1) {
            str_t_setmonth = "0" + str_t_setmonth;
        }
        /**月ごとに月間目標の値を計算して設定 */
        switch (str_t_setmonth) {
            case '03':
            case '04':
            case '05':
            case '06':
            case '07':
            case '08':
                num_s_contractmoney = parseInt(obj_s_record.num_上期契約売上目標金額.value, 10) / num_s_monthfirst;
                num_s_interiormoney = parseInt(obj_s_record.num_上期内装売上目標金額.value, 10) / num_s_monthfirst;
                num_s_matternumber = parseInt(obj_s_record.num_上期案内目標件数.value, 10) / num_s_monthfirst;
                num_s_contractnumber = parseInt(obj_s_record.num_上期契約目標件数.value, 10) / num_s_monthfirst;
                num_s_interiornumber = parseInt(obj_s_record.num_上期内装目標件数.value, 10) / num_s_monthfirst;
               break;
            case '09':
            case '10':
            case '11':
            case '12':
            case '01':
            case '02':
                num_s_contractmoney = parseInt(obj_s_record.num_下期契約売上目標金額.value, 10) / num_s_monthsecond;
                num_s_interiormoney = parseInt(obj_s_record.num_下期内装売上目標金額.value, 10) / num_s_monthsecond;
                num_s_matternumber = parseInt(obj_s_record.num_下期案内目標件数.value, 10) / num_s_monthsecond;
                num_s_contractnumber = parseInt(obj_s_record.num_下期契約目標件数.value, 10) / num_s_monthsecond;
                num_s_interiornumber = parseInt(obj_s_record.num_下期内装目標件数.value, 10) / num_s_monthsecond;
                break;
        }
 
        /**レコード登録用のパラメーター作成 */
        obj_s_records.push(
            {
                "usr_営業担当者": {
                    "value": obj_s_userval
                },
                "num_月間契約売上目標金額": {
                    "value": num_s_contractmoney
                },
                "num_月間内装売上目標金額": {
                    "value": num_s_interiormoney
                },
                "num_月間案内目標件数": {
                    "value": num_s_matternumber
                },
                "num_月間契約目標件数": {
                    "value": num_s_contractnumber
                },
                "num_月間内装目標件数": {
                    "value": num_s_interiornumber
                },
                "dat_対象年月bs": {
                    "value": str_t_setyear + "-" + str_t_setmonth + "-" + str_s_setday
                },
                "lok_予算情報取得": {
                    "value": obj_s_budgetid
                },
            });
        num_s_setmonth++;
    // レコード作成するのは2月まで（num_s_setmonthが3になったらループ終了）
    }while(num_s_setmonth != 3)

    str_s_parm['records'] = obj_s_records;
    /**
     * 予実管理に新規でレコード登録
     */
    kintone.api(kintone.api.url('/k/v1/records', true), 'POST', str_s_parm)
        .then(function (obj_a_resp) {
            /**処理なし */
            console.log(obj_a_resp);
        }, function (obj_a_error) {
            /**error */
            /**処理なし */
            console.log(obj_a_error);
        });
    return obj_a_event;
}