import appIds from '../../../common/app_ids'

const cls_g_app = new appIds();

/**
 * 年間予算入力データに紐づく月間予算入力データを削除する
 * @module obj_g_recDelete
 * @param {object} obj_a_event - イベント情報
 */
export async function obj_g_recDelete(obj_a_event) {
    let obj_s_record = obj_a_event.record;
    let obj_s_userval = obj_s_record['usr_営業担当者'].value[0].code;
    let num_s_setyear = parseInt(obj_s_record.num_年度.value, 10);          //対象年
    let num_s_setmonth = parseInt(obj_s_record.drp_作成開始月.value, 10);   //対象月(数値)
    let str_t_setmonth;                                                    //対象月(文字列)
    let str_s_refday;                                                      //対象年月日
    let obj_s_monthrec;                                                    //削除対象レコードID

    /**
     * 月間予算レコード削除データ作成
     */
    do {
        /**セット月数が12を超えたら値を1月にリセット、セット年数を1繰り上げる */
        if (num_s_setmonth > 12) {
            num_s_setmonth = 1;     //月数
            num_s_setyear++;        //年数
        }
        /**数字を文字に変換、月数が1桁の場合先頭に0をつける */
        str_t_setmonth = String(num_s_setmonth);
        if (str_t_setmonth.length == 1) {
            str_t_setmonth = "0" + str_t_setmonth;
        }
        /**
         * 削除対象月間予算レコードID抽出
         */
        str_s_refday = `${num_s_setyear}-${str_t_setmonth}-01`;
        obj_s_monthrec = await obj_s_getMonthRecInfo(str_s_refday, obj_s_userval);

        /**
         * 月間実績レコード削除
         */
        let obj_s_resp = await vd_s_putMonthProg(obj_s_monthrec);

        num_s_setmonth++;
        // レコード削除するのは2月まで（num_s_setmonthが3になったらループ終了）
    } while (num_s_setmonth != 3)

    return obj_a_event;
}

/**
 * 月間実績レコード情報取得
 * @module obj_s_getMonthRecInfo
 * @param {String} str_a_refday -基準日
 * @param {Object} obj_a_param -レコードの更新情報
 * @return {Object} -レコード番号
 */
export async function obj_s_getMonthRecInfo(str_a_refday, str_a_user) {

    let obj_s_rtn;
    let obj_s_rqparm = {
        "app": cls_g_app.Forecast(),
        "query": `usr_営業担当者 in ("${str_a_user}") and dat_対象年月bs = "${str_a_refday}"`,
        "totalCount": true
    };

    let obj_s_resp = await kintone.api(kintone.api.url('/k/v1/records', true), 'GET', obj_s_rqparm);

    if (obj_s_resp.totalCount != 0) {
        obj_s_rtn = obj_s_resp.records[0].$id.value;
    }
    return obj_s_rtn;
}

/**
 * 月間実績レコード削除
 * @module vd_s_putMonthProg
 * @param {Number} num_a_id -対象ID
 * @return {} -なし
 */
export async function vd_s_putMonthProg(num_a_id) {

    // let obj_s_rqparm = {
    //   "app": cls_g_app.forecast(),
    //   "id": num_a_id
    // };

    var body = {
        'app': cls_g_app.Forecast(),
        'ids': [num_a_id]
    };

    kintone.api(kintone.api.url('/k/v1/records', true), 'DELETE', body, function (resp) {
        // success
        console.log(resp);
    }, function (error) {
        // error
        console.log(error);
        // kintone.api(kintone.api.url('/k/v1/record', true), 'DELETE', obj_s_rqparm)
        // .then(function (obj_a_resp) {
        //     /**処理なし */
        //     console.log(obj_a_resp);
        // }, function (obj_a_error) {
        //     /**error */
        //     /**処理なし */
        //     console.log(obj_a_error);
    });
    return;
}



