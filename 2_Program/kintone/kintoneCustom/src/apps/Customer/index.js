import { vd_g_DisApplinkFild } from '../../common/AllFieldCtr';
import { vd_g_fieldchg } from '../../common/MasterEdit';

(function () {
  'use strict';

  /**レコード追加画面表示時のイベント */
  kintone.events.on('app.record.detail.show', function (obj_a_event) {
    vd_g_DisApplinkFild();
  });

  /**レコード追加画面表示時のイベント */
  kintone.events.on('app.record.create.show', function (obj_a_event) {
    vd_g_DisApplinkFild();
  });

  /**レコード編集画面表示時のイベント */
  kintone.events.on('app.record.edit.show', function (obj_a_event) {
    vd_g_DisApplinkFild();
  });

  /**レコード編集保存前のイベント */
  kintone.events.on('app.record.edit.submit', function (obj_a_event) {
    vd_g_fieldchg(obj_a_event.record, 'txt_顧客検索用', `${obj_a_event.record.txt_顧客名.value}(ID:${obj_a_event.record.rno_顧客番号.value})`);
    return obj_a_event;
  });
})();