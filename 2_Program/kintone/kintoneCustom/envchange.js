const path = require('path');
const glob = require('glob');
const fs = require("fs");

(async () => {
    const debugflg = !await confirm('本番環境の設定にしますか？(y/[n]):');
    if (!debugflg) {
        if (!await confirm('本番環境の設定になりますが、よろしいですか?(y/[n]):')) {
            console.log('処理を中止します');
            process.exit(1);
        }
    }

    /**設定ファイル読み込み */
    fs.readFile('./src/common/app_ids.js', 'utf8', (err, data) => {
        if (err) return console.log(err);
        let result = data.replace(/const bln_g_DEBUGFLG = \w*;/g, `const bln_g_DEBUGFLG = ${debugflg};`);

        /**読み込み用のtmpファイルを作成して情報読み込み */
        fs.writeFileSync('./src/common/app_ids.js', result, 'utf8', (err) => {
            if (err) return console.log(err);
        });

        /**設定ファイル再読み込み */
        fs.readFile('./src/common/app_ids.js', 'utf8', (err, data) => {
            if (err) return console.log(err);
            /**Nodejs読み込み用にexports化 */
            result = data.replace(/export default/g, 'module.exports =');

            /**読み込み用のtmpファイルを作成して情報読み込み */
            fs.writeFile('./app_ids_tmp.js', result, 'utf8', (err) => {
                if (err) return console.log(err);

                /**basePath配下の各ディレクトリを複数のentryとする */
                const basePath = path.resolve('src', 'apps');
                const entries = glob.sync('**/customize-manifest.json', { cwd: basePath }).reduce(
                    (prev, file) => ({
                        ...prev,
                        [path.dirname(file)]: path.resolve(basePath, file)
                    }),
                    {}
                );

                /**全entryのcustomize-manifestを更新 */
                const appIds = require('./app_ids_tmp.js');
                const appidfunc = new appIds;
                for (let key in entries) {
                    /**customize-manifest読み込み */
                    const jsonObject = JSON.parse(fs.readFileSync(entries[key], 'utf8'));
                    /**customize-manifestのアプリIDを更新 */
                    jsonObject.app = eval(`appidfunc.${key}()`);
                    console.log(jsonObject.app)
                    /**customize-manifest書き込み */
                    let obj_t_wirtedata = JSON.stringify(jsonObject, null, ' ')
                    fs.writeFileSync(entries[key], obj_t_wirtedata);
                }

                /**tmpファイル削除 */
                fs.unlink('./app_ids_tmp.js', (err) => {
                    if (err) return console.log(err);
                });
            });
        });
    });
})();

async function confirm(question) {
    const answer = await readUserInput(question);
    return answer.trim().toLowerCase() === 'y';
};

function readUserInput(question) {
    const readline = require('readline').createInterface({
        input: process.stdin,
        output: process.stdout
    });

    return new Promise((resolve, reject) => {
        readline.question(question, (answer) => {
            resolve(answer);
            readline.close();
        });
    });
}