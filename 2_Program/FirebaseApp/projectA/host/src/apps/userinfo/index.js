
import * as justgage from 'justgage';

(function () {
    'use strict';

    let maxSpend = 10;
    let nowValue = 0.5;

    vd_s_GageCfg(nowValue, maxSpend);

})();

/**
 * 
 */
function vd_s_GageCfg(num_a_nowval, num_a_maxval) {

    let gauge = new JustGage({
        id: "gauge", // the id of the html element
        value: num_a_nowval,
        min: 0,
        //minTxt:"min",
        max: num_a_maxval,
        //maxTxt:"max",
        decimals: 2,
        pointer: true,
        gaugeWidthScale: 0.6,
        textRenderer: function (num_a_nowval) {
            return (num_a_nowval + 'GB/' + num_a_maxval + 'GB');
        },
        customSectors: {
            percents: true,
            ranges: [{
                color: '#ff3b30',
                lo: 0,
                hi: 30
            }, {
                color: '#ff8c00',
                lo: 31,
                hi: 50
            }, {
                color: '#43bf58',
                lo: 51,
                hi: 100
            }]
        },
        counter: true
    });

    // update the value randomly
    // 削除予定
    setInterval(() => {
        gauge.refresh(Math.round(Math.random() * 100) / 10);
    }, 5000)
}