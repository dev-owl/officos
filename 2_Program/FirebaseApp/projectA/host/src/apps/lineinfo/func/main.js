import * as linkage from '../linkage/main';

export async function obj_g_GaugeData(str_a_account) {

    let dat_t_date = new Date();
    let num_t_year = dat_t_date.getFullYear();
    let num_t_month = ('00' + dat_t_date.getMonth() + 1).slice(-2);
    let str_t_fromdat = num_t_year + num_t_month + '01';
    dat_t_date.setMonth(dat_t_date.getMonth() + 1);
    let str_t_todat = num_t_year + num_t_month + dat_t_date.getDate(dat_t_date.setDate(0));

    let obj_t_param = {
        'account': str_a_account,
        'fromDate': str_t_fromdat,
        'toDate': str_t_todat
    }

    /* 現時点の残量を取得する */
    let obj_t_detilinfo = await linkage.vd_g_LinkAcntDetel(obj_t_param);
    obj_t_detilinfo = obj_t_detilinfo.responseDatas[0]
    /* 小数点第2位で切り捨て */
    let str_t_quotagb = Math.floor((obj_t_detilinfo.quota / 1024) / 1024 * 10) / 10;

    /* 基本残量の設定 */
    let str_t_maxquotagb = num_s_BaseQuota(obj_t_detilinfo.planCode);
    let obj_t_detilinfo2 = await linkage.vd_g_LinkQuotaHistory(obj_t_param);

    for (let t_key in obj_t_detilinfo2.quotaHistory) {
        str_t_maxquotagb += Number(obj_t_detilinfo2.quotaHistory[t_key].quota);
    }
    /* 小数点第2位で切り捨て */
    str_t_maxquotagb = Math.floor((str_t_maxquotagb / 1024) / 1024 * 10) / 10;

    let obj_t_rtn = {
        'nowQuota': str_t_quotagb,
        'maxQuota': str_t_maxquotagb
    };
    return obj_t_rtn;
}

function num_s_BaseQuota(str_a_planCode) {

    let num_t_base;

    switch (str_a_planCode) {
        case 'PNLM_60G':
            num_t_base = 60;
            break;
        case 'PNLM_7G':
            num_t_base = 7;
            break;
        case 'PNLM_5G':
            num_t_base = 5;
            break;
        case 'PNLM_3G':
            num_t_base = 3;
            break;
        default:
            num_t_base = 3.6;
            break;
    }

    return num_t_base * 1024 * 1024;
}