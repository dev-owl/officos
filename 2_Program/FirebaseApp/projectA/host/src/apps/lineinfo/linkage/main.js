// const str_s_APIBASEURL = 'https://us-central1-jlba-mypage.cloudfunctions.net/api/';
const str_s_APIBASEURL = 'http://localhost:5001/jlba-mypage/us-central1/api/';

export async function vd_g_LinkQuotaHistory(str_a_parm) {
    let obj_t_rtn;
    let str_t_url = str_s_APIBASEURL + 'QuotaHistory';

    await obj_s_LinkageExe(str_t_url, str_a_parm)
        .then((obj_a_resp) => {
            obj_t_rtn = obj_a_resp;
        });
    return obj_t_rtn;
}

export async function vd_g_LinkAcntDetel(str_a_parm) {
    let obj_t_rtn;
    let str_t_url = str_s_APIBASEURL + 'AcntDetel';

    obj_t_rtn = await obj_s_LinkageExe(str_t_url, str_a_parm);
    return obj_t_rtn;
}

function obj_s_LinkageExe(str_a_url, obj_a_reqparm) {

    return new Promise((obj_a_resolve, obj_a_reject) => {
        $.ajax({
            url: str_a_url + '/',
            type: 'POST',
            dataType: 'json',
            // フォーム要素の内容をハッシュ形式に変換
            data: obj_a_reqparm,
            timeout: 5000,
        })
            .done((obj_a_resp) => {
                obj_a_resolve(obj_a_resp);
            })
            .fail(function (obj_a_resp) {
                obj_a_reject(obj_a_resp);
            });;
    });

}