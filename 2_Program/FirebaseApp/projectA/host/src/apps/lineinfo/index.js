import * as justgage from 'justgage';
import * as func from './func/main';
import * as loader from './htmlcss/loader.html';

(async function () {
    'use strict';

    /* Loding描画の追加 */
    vd_s_GageLoad(false);

    let str_t_lineno = $('#lineno').children('span').text();
    str_t_lineno = str_t_lineno.replace(/-/g, '');
    let obj_t_gaugeinfo = await func.obj_g_GaugeData(str_t_lineno);
    vd_s_GageCfg(obj_t_gaugeinfo.nowQuota, obj_t_gaugeinfo.maxQuota);

    /* Loding描画の削除 */
    vd_s_GageLoad(true);

})();

/**
 * 
 */
function vd_s_GageCfg(num_a_nowval, num_a_maxval) {

    let gauge = new JustGage({
        id: "gauge", // the id of the html element
        value: num_a_nowval,
        min: 0,
        max: num_a_maxval,
        decimals: 2,
        pointer: true,
        gaugeWidthScale: 0.6,
        textRenderer: function (num_a_nowval) {
            return (num_a_nowval + 'GB/' + num_a_maxval + 'GB');
        },
        customSectors: {
            percents: true,
            ranges: [{
                color: '#ff3b30',
                lo: 0,
                hi: 10
            }, {
                color: '#ff8c00',
                lo: 11,
                hi: 30
            }, {
                color: '#43bf58',
                lo: 31,
                hi: 100
            }]
        },
        counter: true
    });
}

function vd_s_GageLoad(bln_a_hidden) {
    if (bln_a_hidden == false) {
        $('#gauge').append(loader);
    } else {
        $('#loader').remove();
    }
}