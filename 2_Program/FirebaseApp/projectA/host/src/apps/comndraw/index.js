
/* jquery使用前提で処理を設定 */
import * as header from './htmlcss/header.html'
import * as footer from './htmlcss/footer.html'
import * as menucss from './htmlcss/menu.css'

(function () {
    'use strict';

    $('#header').append(header);
    $('#footer').append(footer);

    $('#drawer').on('click', function () {
        if ($('#nav').hasClass('in')) {
            vd_s_RemDrawer();
        }else{
            vd_s_AddDrawer();
            $('.main_cover').on('click', function () {
                vd_s_RemDrawer();
            });
        }
    });

    function vd_s_RemDrawer(){
        $('#line1').removeClass('line_1');
        $('#line2').removeClass('line_2');
        $('#line3').removeClass('line_3');
        $('#menutxt').removeClass('txt_none');
        $('#nav').removeClass('in');
        $('.main_cover').removeClass('active');
    }
    function vd_s_AddDrawer(){
        $('#line1').addClass('line_1');
        $('#line2').addClass('line_2');
        $('#line3').addClass('line_3');
        $('#menutxt').addClass('txt_none');
        $('#nav').addClass('in');
        $('.main_cover').addClass('active');
    }

})();