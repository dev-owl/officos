const express = require('express');
const { firebaseConfig } = require('firebase-functions');
const functions = require('firebase-functions');
const obj_s_express = express();

/**
 * functionsのエンドポイント
 */
let api = functions.https.onRequest(obj_s_express);
module.exports = { api };

obj_s_express.use((req, res, next) => {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
    res.header('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, OPTIONS');
    next();
});

