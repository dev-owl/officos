/*************************************************************************
 * npm module読み込み
 *************************************************************************/
const fs = require('fs-extra');
const chokidar = require("chokidar");
const cron = require('node-cron');
const imageSize = require('image-size');
const { execSync } = require('child_process');

/*************************************************************************
 * oOigin module読み込み
 *************************************************************************/
const fnc_LogOutput = require('../common/logout');

/*************************************************************************
 * Const Setting
 *************************************************************************/
const str_s_ALLEXTENS = '.(jpeg|jpg|png|bmp|gif)$'; // 許可する拡張子
const str_s_BASEDIR = '画像保存先';
const str_s_UPDIR = '画像保存先_HP用';
const str_s_CWD = process.cwd();
const str_s_LOGO = str_s_CWD + '\\' + 'logo.png'       /**ロゴ画像 */

/*************************************************************************
 * Config Setting
 *************************************************************************/
/**フォルダ構成上1つ上の階層で設定 */
const watcher = chokidar.watch(`../${str_s_BASEDIR}/`, {
    "ignored": /[\\/\\\\]\./,
    "persistent": true,
    "depth": 20
});

/*************************************************************************
 * functions
 *************************************************************************/
watcher.on('ready', function () {

    /**準備完了 */
    fnc_LogOutput("ready watching...");

    /**ファイルの追加 */
    watcher.on('add', fnc_IMConvert);
    /**ファイルの削除 */
    watcher.on('unlink', fnc_FileRemove);
    /**ファイルの変更 */
    // watcher.on('change', function (obj_a_path) {
    //     console.log(obj_a_path + " changeed.");
    // });
    watcher.on('change', fnc_IMConvert);
    /**フォルダの追加 */
    watcher.on('addDir', fnc_DirAdd);
    /**フォルダの削除 */
    watcher.on('unlinkDir', fnc_DirRemove);

});

cron.schedule('0 0 0,3,6,9,12,15,18,21 * * *', () => {
    let dat_s_date = new Date();
    fs.writeFile('wd', `${dat_s_date.getTime()}`);
});
// cron.schedule('1-10 * * * * *',  () =>{
//     let dat_s_date = new Date();
//     fs.writeFile('wd', `${dat_s_date.getTime()}`);
// });

/*************************************************************************
 * static functions
 *************************************************************************/
/**
 * ウォーターマーク追加
 * @param {String} str_a_path 
 */
const fnc_IMConvert = async (str_a_path) => {

    fnc_LogOutput(`added.：${str_a_path}`);
    let str_s_newname = str_a_path;

    /**スペース入りは処理できないためリネーム */
    if (str_a_path.indexOf(' ') != -1) {
        str_s_newname = str_a_path.replace(/ /g, '');

        await fs.rename(str_a_path, str_s_newname, (obj_a1_err) => {
            if (obj_a1_err) {
                fnc_LogOutput(`ERR ファイル名変換失敗：${obj_a1_err.path}`);
                return;
            };
            fnc_LogOutput(`ファイル名補正：${str_a_path}`);
        });
    }

    /**処理をwait */
    await new Promise(resolve => setTimeout(resolve, 100));

    /**画像ファイルの場合のみ処理実施 */
    if (str_a_path.toLowerCase().match(str_s_ALLEXTENS)) {
        /**ファイルパス設定 */
        let str_t_basefile = str_s_CWD + '\\' + str_s_newname;
        let str_t_distfile = str_t_basefile.replace(str_s_BASEDIR, str_s_UPDIR);
        /**ロゴサイズ設定 */
        let obj_t_imgsize = await imageSize(str_t_basefile);
        let num_t_width = Number(obj_t_imgsize.width);
        let num_t_height = Number(obj_t_imgsize.height);
        let str_t_logosize;
        if (num_t_width > num_t_height) {
            str_t_logosize = `x${Math.round(num_t_height * 0.7)}`;
        }
        else {
            str_t_logosize = `${Math.round(num_t_width * 0.9)}x`;
        }

        /**InageMagick実行 */
        execSync(`convert ${str_s_LOGO} -gravity center -resize ${str_t_logosize} -write mpr:base +delete \( ${str_t_basefile} -write mpr:mask +delete \) mpr:mask mpr:base -compose over -auto-orient -composite ${str_t_distfile}`);
        fnc_LogOutput(`ウォーターマーク作成完了：${str_a_path}`);
    }
}

/**
 * ファイル削除
 * @param {String} str_a_path 
 */
const fnc_FileRemove = (str_a_path) => {

    fnc_LogOutput(`unlinked.：${str_a_path}`);
    /**画像ファイルの場合のみ処理実施 */
    if (str_a_path.toLowerCase().match(str_s_ALLEXTENS)) {
        /**パス設定 */
        let str_t_distfile = str_a_path.replace(str_s_BASEDIR, str_s_UPDIR);
        /**ファイル削除実行 */
        fs.unlink(str_t_distfile, (obj_a1_err) => {
            if (obj_a1_err) {
                fnc_LogOutput(`ERR ファイルなし：${obj_a1_err.path}`);
                return;
            };
            fnc_LogOutput(`ファイルを削除しました：${str_t_distfile}`);
        });
    }
}

/**
 * ディレクトリ作成
 * @param {String} str_a_path 
 */
const fnc_DirAdd = (str_a_path) => {

    fnc_LogOutput(`addedDir.：${str_a_path}`);
    /**パス設定 */
    let str_s_dir = str_a_path.replace(str_s_BASEDIR, str_s_UPDIR);
    /**ディレクトリ作成実行 */
    fs.mkdir(str_s_dir, (obj_a1_err) => {
        if (obj_a1_err) {
            fnc_LogOutput(`ERR ディレクトリ既に存在：${obj_a1_err.path}`);
            return;
        };
        fnc_LogOutput(`ディレクトリを作成しました：${str_s_dir}`);
    });
}

/**
 * ディレクトリ削除
 * @param {String} str_a_path 
 */
const fnc_DirRemove = (str_a_path) => {

    fnc_LogOutput(`removedDir.：${str_a_path}`);
    /**パス設定 */
    let str_s_dir = str_a_path.replace(str_s_BASEDIR, str_s_UPDIR);
    /**ディレクトリ削除実行 */
    fs.remove(str_s_dir, (obj_a1_err) => {
        if (obj_a1_err) {
            fnc_LogOutput(`ERR ディレクトリ削除失敗：${obj_a1_err.path}`);
            return;
        };
        fnc_LogOutput(`ディレクトリを削除しました：${str_s_dir}`);
    });
}