/*************************************************************************
 * npm module読み込み
 *************************************************************************/
 const fs = require('fs-extra');
 const moment = require('moment');


/*************************************************************************
 * functions
 *************************************************************************/
/**
 * ログ出力
 * @param {String} str_a_log 
 */
 module.exports = (str_a_log) => {
    let dat_s_date = moment();
    let str_s_log = `${dat_s_date.format()} ${str_a_log}`;
    console.log(str_s_log);
    /**ログ出力 */
    fs.appendFile('watermark.log', `${str_s_log}\n`);
}