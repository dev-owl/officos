/*************************************************************************
 * npm module読み込み
 *************************************************************************/
const { spawn, exec } = require('child_process');
const fs = require('fs-extra');

/*************************************************************************
 * oOigin module読み込み
 *************************************************************************/
const fnc_LogOutput = require('../common/logout');

/*************************************************************************
 * Const Setting
 *************************************************************************/
const str_CMDLINE = ".\\watchdog.bat";
const num_DIFVAL = ((3 * 60) + 10) * 60 * 1000;    /**3時間10分で設定 */

/*************************************************************************
 * functions
 *************************************************************************/
fs.readFile("wd", { encoding: "utf8" }, (err, file) => {
    if (err) {
        console.error(err);
        console.error('err');
        /**ここで実行 */
        vd_s_runCommand();
    } else {
        let dat_t_date = new Date();
        let num_t_diftime = dat_t_date.getTime() - file
        if (num_t_diftime > num_DIFVAL) {
            /**ここで実行 */
            vd_s_runCommand();
        }
        else {
            fnc_LogOutput("WaterMark.exe起動確認OK.");
        }
    }
});


/**コマンドを外部プロセスとして実行 */
function vd_s_runCommand() {

    fnc_LogOutput("WaterMark.exe再起動...");
    
    let dat_s_date = new Date();
    fs.writeFile('wd', `${dat_s_date.getTime()}`);

    // exec("robocopy .\\画像保存先 .\\画像保存先_HP用 /L /E /XO /Z /R:1 /FFT /TS /XD 除外フォルダ /LOG:.\\フォルダ比較結果.txt", (err, stdout, stderr) => {
    exec("robocopy ..\\画像保存先 ..\\画像保存先_HP用 /L /E /R:1 /FP /LOG:.\\フォルダ比較結果.txt", (err, stdout, stderr) => {
        if (err) {
            console.log(`stderr: ${stderr}`)
            return
        }
        console.log(`stdout: ${stdout}`)
    }
    )
    /**バックグラウンドで実行 */
    /**メインプロセスが終了しても外部プロセスとして動作します。*/
    const obj_s_child = spawn(str_CMDLINE, [], {
        "stdio": 'ignore',  // piping all stdio to /dev/null
        "detached": true,   // メインプロセスから切り離す設定
        "env": process.env, // NODE_ENV を tick.js へ与えるため
    });
    obj_s_child.unref(); // メインプロセスから切り離す
}