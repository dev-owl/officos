
<div style="page-break-before:always"></div>

## 御会社名(業者名)の編集手順

※業者名も同様の手順で修正を行えるため、御会社名を代表に説明します

1. 案件管理アプリの編集対象レコードを開いて、御会社名をクリック
   ![page1](.\\img\page1.png)

2. 御会社情報が表示されるため編集ボタンをクリック
   ![page2](.\\img\page2.png)
   
3. 編集したい内容を修正し、保存ボタンをクリック
   ![page3](.\\img\page3.png)
   ![page4](.\\img\page4.png)
   
<div style="page-break-before:always"></div>
   ※保存をクリックした後に、元のレコード情報の更新が完了すると下記メッセージが表示され元のレコード情報が更新されます。
   
   （更新されていない場合は案件管理のレコードを再読み込みしてみてください）
   ![page5](.\\img\page5.png)
   ![page6](.\\img\page6.png)

